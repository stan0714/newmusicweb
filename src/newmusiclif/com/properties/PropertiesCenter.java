package newmusiclif.com.properties;


import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;






import javax.servlet.http.HttpServlet;

import newmusiclife.com.dao.PropertiesDao;
import newmusiclife.com.vo.PropertiesVO;


/*
 * 讀取外部的設定檔案
 */

public class PropertiesCenter extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static boolean debug = true;
	private static HashMap<String,String> map;
	private static Properties configProp = null;
	//String file="/com/tej/Properties/config.properties";
	 public void init()
	 {
		 	map=new HashMap<String, String>();
	    	loadProperties(); 
	 }
	 
//	 public void setFile(String file){
//		 this.file=file;
//	 }
		/*
		 * 讀取外部檔案
		 */
	private void loadProperties() {
		
		PropertiesUtil.getInstance().loadProperties();
	}

	
	/*
	 * 傳入Key值 將Prop中的Value取出回傳
	 */
	public static String getPropertiesValue(String key) {
		return configProp.getProperty(key);
	}
	/*
	 * 回傳賭取完成的Prop檔案
	 */
	public  Properties returnProperties() {
		return configProp;
	}
	
	/*
	 * 將讀取的Prop內容讀書
	 */
	public void listAllKey(){
		  Enumeration<?> em = configProp.keys();
		  while(em.hasMoreElements()){
		  String str = (String)em.nextElement();
		  System.out.println("KEY:"+str + " Value: " + configProp.get(str));
		  }
	}
}
