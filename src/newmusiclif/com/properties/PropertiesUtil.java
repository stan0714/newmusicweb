package newmusiclif.com.properties;

import java.util.HashMap;
import java.util.List;

import newmusiclife.com.dao.PropertiesDao;
import newmusiclife.com.vo.PropertiesVO;

public class PropertiesUtil {
	private  HashMap<String,String> map=new HashMap<String, String>();
	List<PropertiesVO> PropertiesList;
	private static PropertiesUtil instance;
	
	private PropertiesUtil(){
		loadProperties();
	}
	
	public static PropertiesUtil getInstance(){
		if(instance==null)
			instance=new PropertiesUtil();

			return instance;
		
	}
	
	
	public static boolean  updateProperties(){
		try{
		if(instance==null)
			instance=new PropertiesUtil();

			instance.loadProperties();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	
	public void loadProperties(){
		PropertiesList=PropertiesDao.getInstance().selectAll();
		for (PropertiesVO vo: PropertiesList) {
			map.put(vo.getName(), vo.getValue());
		}
	}

	public String getPropertiy(String key){
		return map.get(key);
	}
}
