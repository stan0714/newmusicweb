package tw.com.tej.tools;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeTools {

	
	public static String convertDateTOYYYYMMDD(String time) throws ParseException{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String result = format.format(format.parse(time));
		return result;
	}
	public static Timestamp convertStringtoTimestamp(String date) throws ParseException{
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date formateDate = formatter.parse(date);
//		java.sql.Timestamp timestamp = new java.sql.Timestamp(formateDate.getTime());
		return new java.sql.Timestamp(formateDate.getTime());
	}
//	/**
//	 * @param args
//	 * @throws ParseException 
//	 */
//	public static void main(String[] args) throws ParseException {
//
//
//		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//		String reformattedStr = format.format(format.parse(time));
//		System.out.println(reformattedStr);
//
//	}

}
