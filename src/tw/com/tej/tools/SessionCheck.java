package tw.com.tej.tools;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import javax.naming.NamingException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;



@Path("/session")
public class SessionCheck {
	@GET
	@Path("/check")
	@Consumes("application/json")
	@Produces(MediaType.APPLICATION_JSON)
	public String requestVersionAndTicket (@Context HttpServletRequest request,@Context HttpServletResponse res ,InputStream requestBodyStream
			) throws JSONException, UnsupportedEncodingException, NamingException {
		JSONObject respJSON= new JSONObject();
		HttpSession session = request.getSession();
		String sessionid=null;
		Object sessionidObject = session.getAttribute("sessionid");
		if(sessionidObject!=null){
			sessionid=sessionidObject.toString();
			session.removeAttribute("sessionid");
		}
        if(sessionid!=null){
        	respJSON.put("status", 200);
        }else{
        	respJSON.put("status", 404);
        }

		return respJSON.toString();
	}
	
	public String getSessionCookie( Cookie[] cookies){
		String sessionid = null;
	    if (cookies != null) {
		      for (int i = 0; i < cookies.length; i++) {
		    	  System.out.println(cookies[i].getName());
		        if (cookies[i].getName().equals("sessionid")) {
		          sessionid = cookies[i].getValue();
		          break;
		        }
		      }
		   }
	    return sessionid;
		
	}

}
