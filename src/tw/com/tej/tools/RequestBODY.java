package tw.com.tej.tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

public class RequestBODY {
	
	static public String get(InputStream is) {
		StringBuffer buffer = new StringBuffer();
	
		int bufferContent = 0;
		do
		{
			try {
				bufferContent = is.read();
				if(bufferContent > 0){
					buffer.append((char) bufferContent);
				}
				
			} catch (IOException e) {

				e.printStackTrace();
			}
		}while(bufferContent > 0 );
		return buffer.toString();
	}

	
	static public String get(InputStream is,Charset charset)  {

		if (is != null) {
			  StringBuffer buffer = new StringBuffer();
			    try {
				InputStreamReader isr = new InputStreamReader(is,charset);
				Reader in = new BufferedReader(isr);
				int ch;
				while ((ch = in.read()) > -1) {
					buffer.append((char)ch);
				}
				in.close();
					return buffer.toString();
			    } catch (IOException e) {
			    	e.printStackTrace();
			    	return null;
			    }

		} else {
			return "";
		}
	}
}
