package tw.com.tej.tools;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class SecurityFilter
 */
public class SecurityFilter implements Filter {

    /**
     * Default constructor. 
     */
    public SecurityFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;  
	    HttpServletResponse res = (HttpServletResponse) response;  
	    HttpSession session = req.getSession();
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
	    if (session.getAttribute("user") != null&&session.getAttribute("sessionid") !=null) {  
	        chain.doFilter(request, response);  
	    } else {  
			response.setContentType("text/html");
			response.setCharacterEncoding("UTF-8");
	    	PrintWriter out = response.getWriter();
	    	out.println("查無登入資訊.三秒鐘後導回登入頁面!");
	        res.setHeader("Refresh", "3; " + "../login.jsp");
	    }  
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
