package com.tej.library;

import java.io.File;
import java.io.IOException;

import org.ini4j.Wini;

public class IniClass {

	/**
	 * @param args
	 * @throws IOException
	 * @throws InvalidFileFormatException
	 */
	String path = "D:\\ini" + File.separator;
	static Wini ini;

	public IniClass(String filename) throws IOException {
		readPro(filename);
	}

	public void readPro(String filename) throws IOException {
		this.ini = new Wini(new File(path + filename));
	}

	public void setPath(String path) throws IOException {
		this.path = path;
	}

	public Wini getIni() {
		return ini;
	}

}
