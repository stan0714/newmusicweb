package com.tej.library;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.ini4j.Wini;

public class DbIni {
	String server;
	int port;
	String username;
	String dbname;
	String scode_table;
	Wini ini=null;
	DbIni(String filename)throws IOException{
		if(new File(filename).exists()){
			ini=new IniClass(filename).ini;
			initArgs();
		}
	}
	
	public DbIni()throws IOException{		
			ini=new IniClass("db.ini").getIni();
			if(ini.get("DB")!=null){
				initArgs();
			}
	}
	
	private void setusername(String username){
		this.username=username;
	}
	
	private void setDBname(String dbname){
		this.dbname=dbname;
	}
	
	public void initArgs(){
		String DBkeyword="DB";
		this.server=ini.get(DBkeyword,"server");
		this.port=Integer.valueOf(ini.get(DBkeyword,"port"));
		this.username=ini.get(DBkeyword,"username");
		this.dbname=ini.get(DBkeyword,"dbname");
		this.scode_table=ini.get(DBkeyword, "scodetable");
	}
	public HashMap<String, Object> getConnect(){
		HashMap<String, Object> key=new HashMap<String, Object>();
		key.put("server", server);
		key.put("port", port);
		key.put("username", username);
		key.put("dbname", dbname);
		return key;		
	}
	
	public String getServer(){
		return server;
	}
	public int getPort(){
		return port;
	}
	public String getUser(){
		return username;
	}
	public String getDBname(){
		return dbname;
	}
	public String getscodeTable(){
		return scode_table;
	}
	

}
