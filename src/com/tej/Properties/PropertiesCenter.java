package com.tej.Properties;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;






import javax.servlet.http.HttpServlet;

import newmusiclife.com.dao.PropertiesDao;
import newmusiclife.com.vo.PropertiesVO;


/*
 * 讀取外部的設定檔案
 */

public class PropertiesCenter extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static boolean debug = true;
	private HashMap<String,String> map=new HashMap<String, String>();
	private static Properties configProp = null;
	//String file="/com/tej/Properties/config.properties";
	 public void init()
	 {
	    	loadProperties(); 
	 }
	 
//	 public void setFile(String file){
//		 this.file=file;
//	 }
		/*
		 * 讀取外部檔案
		 */
	private void loadProperties() {
		
		configProp= new Properties();
		try {
			
			List<PropertiesVO> propertiesList=PropertiesDao.getInstance().selectAll();
			for (int i = 0; i < propertiesList.size(); i++) {
				PropertiesVO vo=propertiesList.get(i);
				System.out.println(i+":"+vo.getName()+" value:"+vo.getValue());
				if(vo.getName()!=null){
					map.put(vo.getName(), vo.getValue());
				}
			}
//			InputStream in = this.getClass().getClassLoader().getResourceAsStream(file);
//			Reader reader = new InputStreamReader(in, "UTF-8");
//			//			fis = new FileInputStream(file);
//			configProp.load(reader);
//			in.close();

//		} catch (FileNotFoundException e) {
//
////			e.printStackTrace();
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	public String getPropertValue(String key){
		if(map.containsKey(key)){
			return map.get(key);
		}else{
			return null;
		}
	}
	
	/*
	 * 傳入Key值 將Prop中的Value取出回傳
	 */
	public static String getPropertiesValue(String key) {
		return configProp.getProperty(key);
	}
	/*
	 * 回傳賭取完成的Prop檔案
	 */
	public  Properties returnProperties() {
		return configProp;
	}
	
	/*
	 * 將讀取的Prop內容讀書
	 */
	public void listAllKey(){
		  Enumeration<?> em = configProp.keys();
		  while(em.hasMoreElements()){
		  String str = (String)em.nextElement();
		  System.out.println("KEY:"+str + " Value: " + configProp.get(str));
		  }
	}
}
