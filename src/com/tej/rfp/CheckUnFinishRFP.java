package com.tej.rfp;

import java.awt.image.TileObserver;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TimerTask;

import javax.mail.MessagingException;

import org.apache.log4j.Logger;

import com.tej.Properties.PropertiesCenter;
import com.tej.db.JDBCtoPostgres;
import com.tej.mail.JavaMailforTEJ;

public class CheckUnFinishRFP extends TimerTask {
	private Logger log = Logger.getLogger("rfp");



	@Override
	public void run() {
		 log.info("[Unfinsihed JOB]Check unfinish job start!");
		JDBCtoPostgres db = new JDBCtoPostgres();
		String yesterday = getYesterdaDateString();
		try {
			HashMap<String, ArrayList<UnfinishObject>> checkreult = db
					.checkUnfinishRFP(yesterday);
			
			Iterator iter = checkreult.entrySet().iterator();
			String title=PropertiesCenter.getPropertiesValue("unfinishmailtitle");
			while (iter.hasNext()) {
			    Map.Entry entry = (Map.Entry) iter.next();
			    String key = (String)entry.getKey();
			    Object val = entry.getValue();
			    String[] person=key.split("-");
			    JavaMailforTEJ mail=new JavaMailforTEJ();
			    mail.setMailReciver(person[person.length-1]);
			    String message=replaceContext(person[1],(ArrayList<UnfinishObject>)val);
			    mail.sendMail(title, message);
			    log.info("[Unfinsihed JOB] Mail to "+person[1]+" Title:"+title+" Message:"+message);
			} 
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			db.exitSQLConnect();
		}
		log.info("[Unfinsihed JOB]Check unfinish job done!");
		// TODO Auto-generated method stub

	}
	//親愛的@xxx@您好:<br>您目前手上有:<br>@content@,<br>已經過期(以系統端時間為主)<br>請您盡快處理<br>謝謝
	public String replaceContext(String user ,ArrayList<UnfinishObject> job){
		String context=PropertiesCenter.getPropertiesValue("unfinishmailcontext");
		context=context.replace("@xxx@", user).replace("@content@", getunfinishwork(job));
		return context;
	}
	/*
	 * 將未完成的工作信件內容組合
	 */
	private String getunfinishwork(ArrayList<UnfinishObject> job){
		String context=PropertiesCenter.getPropertiesValue("unfinishmailcontextvalue");
		String result="";
		for (int i = 0; i < job.size(); i++) {
			UnfinishObject temp=job.get(i);
			String hopeDate="無資訊";
			if(!temp.getHopefinish_date().equals("")){
				hopeDate=temp.getHopefinish_date();
			}
			String mailContext=context.replace("@ccc@",temp.getNumber()).replace("@zzz@", temp.getStatus()).replace("@date@", temp.getEstimate_date()).replace("@hopedate@", hopeDate);
			result=result+mailContext;
		}
		return result;
	}
	
	
	private String getYesterdaDateString() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		return dateFormat.format(cal.getTime());
	}

}
