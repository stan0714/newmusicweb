package com.tej.rfp;

public class UnfinishObject {
	private String number;
	private String status;
	private String estimate_date;
	private String hope_finish_date="";
	private boolean is_question;
	/**
	 * @param args
	 */
	public UnfinishObject(String number,String date,String status,boolean question,String hopedate){
		this.number=number;
		this.estimate_date=date;
		this.status=status;
		this.is_question=question;
		if(hopedate!=null){
			this.hope_finish_date=hopedate;
		}
	}
	
	public String getNumber(){
		return number;
		
	}
	
	public String getEstimate_date(){
		return estimate_date;
		
	}
	public String getHopefinish_date(){
		return hope_finish_date;
		
	}
	public String getStatus(){
		return status;
		
	}
	
	public boolean getIs_quest(){
		return is_question;
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
