package com.tej.rfp;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.ArrayList;

import javax.naming.NamingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tej.Properties.PropertiesCenter;
import com.tej.db.JDBCtoPostgres;

import tw.com.tej.tools.RequestBODY;

@Path("/rfpnumber")
public class rfpnumber {
	private Logger log = Logger.getLogger("rfp");
	@POST
	@Path("/check")
	@Consumes("application/json")
	@Produces(MediaType.APPLICATION_JSON)
	public String requestRFPNumberAndCheck(@Context HttpServletRequest request,
			@Context HttpServletResponse res, InputStream requestBodyStream)
			throws JSONException, UnsupportedEncodingException, NamingException {

		JSONObject requestJSON = new JSONObject(RequestBODY.get(
				requestBodyStream, Charset.forName("utf-8")));
		String number = requestJSON.getString("rfp_number");
		JDBCtoPostgres jdbCtoPostgres=new JDBCtoPostgres();
		boolean checkResult=jdbCtoPostgres.checkRfpNumber(number);
		jdbCtoPostgres.exitSQLConnect();
		JSONObject respJSON = new JSONObject();
		respJSON.put("status",200);
		respJSON.put("result",checkResult);
		
		return respJSON.toString();

	}
	
	@POST
	@Path("/insert")
	@Consumes("application/json")
	@Produces(MediaType.APPLICATION_JSON)
	public String insertRFPNumberInfo(@Context HttpServletRequest request,
			@Context HttpServletResponse res, InputStream requestBodyStream)
			throws JSONException, UnsupportedEncodingException, NamingException, ParseException {

		JSONObject requestJSON = new JSONObject(RequestBODY.get(
				requestBodyStream, Charset.forName("utf-8")));
		//擷取前端傳送的資訊
		String number = requestJSON.getString("rfp_number");
		String person=requestJSON.getString("person").split("_")[0];
		String date=requestJSON.getString("date");
		String content=requestJSON.getString("content");
		int hours=requestJSON.getInt("hours");
		boolean type=requestJSON.getBoolean("type");
		//取出登入的使用者
	    HttpSession session = request.getSession();  
	    String login_user=(String)session.getAttribute("user");
		JDBCtoPostgres jdbCtoPostgres=new JDBCtoPostgres();
		//新增RFP單號資料
		jdbCtoPostgres.insertNewRfpNumber(number, type, content, date, hours);
		log.info("User:"+login_user +" insert rfp number "+number+" type is "+type+" content "+content+" date:"+date+" hours:"+hours);
		//新增一筆單號初始資料
		JSONArray personArray=new JSONArray();
		personArray.put(person);
		jdbCtoPostgres.inserRfpNumber(number, login_user, personArray,0,"A",0);
		log.info("User:"+login_user +" insert rfp number "+number+" to "+person+" status is A");
		jdbCtoPostgres.exitSQLConnect();
		JSONObject respJSON = new JSONObject();
		respJSON.put("status",200);
		
		return respJSON.toString();

	}
	
	@POST
	@Path("/insertMuti")
	@Consumes("application/json")
	@Produces(MediaType.APPLICATION_JSON)
	public String insertMutiRFPNumberInfo(@Context HttpServletRequest request,
			@Context HttpServletResponse res, InputStream requestBodyStream)
			throws JSONException, UnsupportedEncodingException, NamingException, ParseException {

		JSONObject requestJSON = new JSONObject(RequestBODY.get(
				requestBodyStream, Charset.forName("utf-8")));
		//擷取前端傳送的資訊
		String number = requestJSON.getString("rfp_number");
		JSONArray person=requestJSON.getJSONArray("person");
		int od_number=requestJSON.getInt("odnumber");
		String startdate=requestJSON.getString("startdate");
		String enddate=requestJSON.getString("enddate");
		String content=requestJSON.getString("content");
		int hours=requestJSON.getInt("hours");
		String newstatus=requestJSON.getString("newstatus").split("_")[0];
		//取出登入的使用者
	    HttpSession session = request.getSession();  
	    String login_user=(String)session.getAttribute("user");  
		JDBCtoPostgres jdbCtoPostgres=new JDBCtoPostgres();
//		//更新原本的OD資訊
		System.out.println("User:"+login_user +" change rfp number "+number+" to "+person.toString()+" status is "+newstatus);
		jdbCtoPostgres.updateRfpNumber(number, od_number, login_user, content, startdate,enddate, hours);
//		String[] personArray={person};
		if(person.length()>0){
			jdbCtoPostgres.inserRfpNumber(number, login_user, person,0,newstatus,od_number);
		}else{
			
			jdbCtoPostgres.completeRfpNumber(number, login_user, newstatus, content, enddate, hours,od_number);
		}
		log.info("User:"+login_user +" change rfp number "+number+" to "+person.toString()+" status is "+newstatus);
		jdbCtoPostgres.exitSQLConnect();
		JSONObject respJSON = new JSONObject();
		respJSON.put("status",200);
		
		return respJSON.toString();

	}
	
	
	@GET
	@Path("/get")
	@Consumes("application/json")
	@Produces(MediaType.APPLICATION_JSON)
	public String requestRFPNumberByperson(@Context HttpServletRequest request,
			@Context HttpServletResponse res, InputStream requestBodyStream)
			throws JSONException, UnsupportedEncodingException, NamingException {
		HttpServletRequest req = (HttpServletRequest) request;    
	    HttpSession session = req.getSession();  
	    String login_user=(String)session.getAttribute("user");
	    JSONObject respJSON = new JSONObject();
	    if(login_user!=null){
		JSONObject requestJSON = new JSONObject(RequestBODY.get(
				requestBodyStream, Charset.forName("utf-8")));
		String number = requestJSON.getString("rfp_number");
		JDBCtoPostgres jdbCtoPostgres=new JDBCtoPostgres();
		boolean checkResult=jdbCtoPostgres.checkRfpNumber(number);
		jdbCtoPostgres.exitSQLConnect();
		
		respJSON.put("status",200);
		respJSON.put("result",checkResult);
	    }else{
	    	respJSON.put("status",404);
	    }
		
		return respJSON.toString();

	}
	@GET
	@Path("/queryunfinish")
	@Consumes("application/json")
	@Produces(MediaType.APPLICATION_JSON)
	public String request(@Context HttpServletRequest request,
			@Context HttpServletResponse res, InputStream requestBodyStream)
			throws JSONException, UnsupportedEncodingException, NamingException, ParseException {
		HttpServletRequest req = (HttpServletRequest) request;    
	    HttpSession session = req.getSession();  
	    String login_user=(String)session.getAttribute("user");
	    JSONObject respJSON = new JSONObject();
	    if(login_user!=null){
		JDBCtoPostgres jdbCtoPostgres=new JDBCtoPostgres();
		String worklist=jdbCtoPostgres.GetRFPworkbyUser(login_user);
		jdbCtoPostgres.exitSQLConnect();
		
		respJSON.put("status",200);
		respJSON.put("array",worklist);
	    }else{
	    	respJSON.put("status",404);
	    }
		
		return respJSON.toString();

	}
	
	@POST
	@Path("/querybyvalues")
	@Consumes("application/json")
	@Produces(MediaType.APPLICATION_JSON)
	public String requestbyNumberandStatus(@Context HttpServletRequest request,
			@Context HttpServletResponse res, InputStream requestBodyStream)
			throws JSONException, UnsupportedEncodingException, NamingException, ParseException { 
		JSONObject requestJSON = new JSONObject(RequestBODY.get(
				requestBodyStream, Charset.forName("utf-8")));
		String number = requestJSON.getString("rfp_number");
		String status = requestJSON.getString("status");
		int od = requestJSON.getInt("od");
	    JSONObject respJSON = new JSONObject();
		JDBCtoPostgres jdbCtoPostgres=new JDBCtoPostgres();
		String worklist=jdbCtoPostgres.GetRFPworkbyWorkNumberandStatus(number,status,od);
		ArrayList<String> list=jdbCtoPostgres.getRfpNumberInfo(number);
		jdbCtoPostgres.exitSQLConnect();
		respJSON.put("status",200);
		respJSON.put("array",worklist);
		respJSON.put("detail",list.toArray());	
		return respJSON.toString();

	}
	
	/*
	 * 得到設定檔案(config.properties)關於說明檔案件例說明提示
	 */
	@GET
	@Path("/getcreatenumberhint")
	@Consumes("application/json")
	@Produces(MediaType.APPLICATION_JSON)
	public String getcreatenumberhint(@Context HttpServletRequest request,
			@Context HttpServletResponse res, InputStream requestBodyStream)
			throws JSONException, UnsupportedEncodingException, NamingException {
		String hint=PropertiesCenter.getPropertiesValue("rfp_number_hint");;
	    JSONObject respJSON = new JSONObject();
		
		respJSON.put("status",200);
		respJSON.put("hint",hint);

		
		return respJSON.toString();

	}
	
	
	
	@POST
	@Path("/updatecomment")
	@Consumes("application/json")
	@Produces(MediaType.APPLICATION_JSON)
	public String updatecomment(@Context HttpServletRequest request,
			@Context HttpServletResponse res, InputStream requestBodyStream)
			throws JSONException, UnsupportedEncodingException, NamingException, ParseException { 
		JSONObject requestJSON = new JSONObject(RequestBODY.get(
				requestBodyStream, Charset.forName("utf-8")));
		String number = requestJSON.getString("rfp_number");
		int od = requestJSON.getInt("od");
		String content=requestJSON.getString("content");
		HttpSession session = request.getSession();
		String login_user=(String)session.getAttribute("user");
	    JSONObject respJSON = new JSONObject();
		JDBCtoPostgres jdbCtoPostgres=new JDBCtoPostgres();
		jdbCtoPostgres.updateCommentbyRfpNumber(number, od, content);
		jdbCtoPostgres.exitSQLConnect();
		log.info("User:"+login_user +" change comment:"+content+" for rfp number: "+number+" od: "+od);
		respJSON.put("status",200);
		return respJSON.toString();

	}
	
	
	@POST
	@Path("/querybynumber")
	@Consumes("application/json")
	@Produces(MediaType.APPLICATION_JSON)
	public String requestbyNumber(@Context HttpServletRequest request,
			@Context HttpServletResponse res, InputStream requestBodyStream)
			throws JSONException, UnsupportedEncodingException, NamingException, ParseException { 
		JSONObject requestJSON = new JSONObject(RequestBODY.get(
				requestBodyStream, Charset.forName("utf-8")));
		String number = requestJSON.getString("rfp_number");
	    JSONObject respJSON = new JSONObject();
		JDBCtoPostgres jdbCtoPostgres=new JDBCtoPostgres();
		String worklist=jdbCtoPostgres.GetRFPworkbyWorkNumber(number);
		ArrayList<String> list=jdbCtoPostgres.getRfpNumberInfo(number);
		jdbCtoPostgres.exitSQLConnect();
		respJSON.put("status",200);
		respJSON.put("array",worklist);
		respJSON.put("detail",list.toArray());

		
		return respJSON.toString();

	}
	
	@GET
	@Path("/autocom")
	@Consumes("application/json")
	@Produces(MediaType.APPLICATION_JSON)
	public String requestVersionAndTicket (@Context HttpServletRequest request, InputStream requestBodyStream
			) throws JSONException {
		JDBCtoPostgres sql=new JDBCtoPostgres();
		ArrayList<String> command=sql.GETRfpNumberForAutoCom();
		JSONObject respJSON = new JSONObject();
		if(command.size()!=0){
			respJSON.put("status", 200);
			respJSON.put("array", command);
		}else{
			respJSON.put("status", 404);
		}
		return respJSON.toString();
	}
	
}
