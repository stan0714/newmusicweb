package com.tej.person;


import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.naming.NamingException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import com.tej.db.JDBCtoPostgres;

/*
 * 主要是處理關於人員的Servlet 
 * get:將資料庫的理所有人員取出 並依照不們回傳Array到前端
 */

@Path("/person")
public class GetPerson {
		@GET
		@Path("/get")
		@Consumes("application/json")
		@Produces(MediaType.APPLICATION_JSON)
		public String requestVersionAndTicket(
				@Context HttpServletRequest request, InputStream requestBodyStream)
				throws JSONException, UnsupportedEncodingException,
				NamingException, SQLException, ClassNotFoundException {
			JDBCtoPostgres con=new JDBCtoPostgres();
			JSONObject respJSON = new JSONObject();
			HashMap<String, ArrayList<employee_info>>map=con.getUserlist();
			Iterator<?> iter = map.entrySet().iterator();
			JSONArray departArray=new JSONArray();
			JSONArray personArray=new JSONArray();
			JSONArray mailArray=new JSONArray();
			JSONArray idArray=new JSONArray();
			while (iter.hasNext()) {	
				@SuppressWarnings("rawtypes")
				Map.Entry entry = (Map.Entry) iter.next(); 
				departArray.put((String)entry.getKey());
				 @SuppressWarnings("unchecked")
				ArrayList<employee_info> list=(ArrayList<employee_info>)entry.getValue();
				 ArrayList<String> person=new ArrayList<String>();
				 ArrayList<String> mail=new ArrayList<String>();
				 ArrayList<String> id=new ArrayList<String>();
				for (int i = 0; i < list.size(); i++) {
					employee_info temp=list.get(i);
					person.add(temp.getName());
					mail.add(temp.getMail());
					id.add(temp.getAccount());
				}
				personArray.put(person.toArray());
				mailArray.put(mail.toArray());
				idArray.put(id.toArray());
			}
			respJSON.put("dep", departArray);
			respJSON.put("person", personArray);
			respJSON.put("mail", mailArray);
			respJSON.put("id", idArray);
			return respJSON.toString();
		}

	}

