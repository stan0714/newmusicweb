package com.tej.mail;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import java.nio.charset.Charset;


import javax.mail.MessagingException;
import javax.naming.NamingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tej.Properties.PropertiesCenter;

import tw.com.tej.tools.RequestBODY;

/*
 * 主要是處理關於寄信相關的Servlet 
 * send:只傳給一個人
 * sendMuti:寄信通知多人
 */

@Path("/mail")
public class MailServelt {
	private Logger log = Logger.getLogger("rfp");
	@POST
	@Path("/send")
	@Consumes("application/json")
	@Produces(MediaType.APPLICATION_JSON)
	public String sendMail(@Context HttpServletRequest request,
			@Context HttpServletResponse res, InputStream requestBodyStream)
			throws JSONException, UnsupportedEncodingException, NamingException, MessagingException {

		JSONObject requestJSON = new JSONObject(RequestBODY.get(
				requestBodyStream, Charset.forName("utf-8")));
		String number = requestJSON.getString("rfp_number");
		String person=requestJSON.getString("person");
		String personmail=requestJSON.getString("mail");
		String newstatus=requestJSON.getString("newstatus");
	    HttpSession session = request.getSession();  
	    String login_user=(String)session.getAttribute("user");
	    String mailtitle=PropertiesCenter.getPropertiesValue("newmailtitle");
	    String context=replaceContext(person, number, newstatus, login_user);
	    JavaMailforTEJ mail=new JavaMailforTEJ();
	    mail.setMailReciver(personmail);
	    mail.sendMail(mailtitle, context);
	    log.info("User:"+login_user+" send mail to "+person+" about RFP number"+number +" sttus is "+newstatus);
		JSONObject respJSON = new JSONObject();
		respJSON.put("status",200);		
		return respJSON.toString();

	}
	
	@POST
	@Path("/sendMuti")
	@Consumes("application/json")
	@Produces(MediaType.APPLICATION_JSON)
	public String sendMailToMutiUser(@Context HttpServletRequest request,
			@Context HttpServletResponse res, InputStream requestBodyStream)
			throws JSONException, UnsupportedEncodingException, NamingException, MessagingException {

		JSONObject requestJSON = new JSONObject(RequestBODY.get(
				requestBodyStream, Charset.forName("utf-8")));
		String number = requestJSON.getString("rfp_number");
		JSONArray person=requestJSON.getJSONArray("person");
		JSONArray personmail=requestJSON.getJSONArray("mail");
//		String enddate=requestJSON.getString("enddate");
//		String content=requestJSON.getString("content");
		String newstatus=requestJSON.getString("newstatus");
	    HttpSession session = request.getSession();  
	    String login_user=(String)session.getAttribute("user");
	    String mailtitle=PropertiesCenter.getPropertiesValue("newmailtitle");
	    log.info("User:"+login_user+" send mail to "+person.toString()+" about RFP number"+number +" sttus is "+newstatus);
	    for (int i = 0; i < person.length(); i++) {
	    	String context=replaceContext(person.getString(i), number, newstatus, login_user);
	    	JavaMailforTEJ mail=new JavaMailforTEJ();
	    	mail.setMailReciver(personmail.getString(i));
	    	mail.sendMail(mailtitle, context);
	    }
		JSONObject respJSON = new JSONObject();
		respJSON.put("status",200);		
		return respJSON.toString();

	}
	/*
	 * 讀取Config中Key值為newmailcontext 並使用取代的動作將信件內文作出
	 */
	
	//親愛的@xxx@您好:<br>目前工作單編號:@ccc@<br>狀態:@zzz@<br>由申請人:@yyy@指派給您,請您盡快處理<br>謝謝
	public String replaceContext(String user,String number,String status,String applyuser){
		String context=PropertiesCenter.getPropertiesValue("newmailcontext");
		context=context.replace("@xxx@", user).replace("@ccc@", number).replace("@zzz@", status).replace("@yyy@", applyuser);
		return context;
	}
	
}
