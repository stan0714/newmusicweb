package com.tej.mail;

import javax.mail.PasswordAuthentication;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;

import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;

import javax.mail.internet.MimeMessage;


public class JavaMailforTEJ {
	private String user = "tejschool";
	private String passwd = "tejschool123";
	private String SMTP_HOST_NAME = "mail.tej.com.tw";
	private final String SMTP_PORT = "25";
	private  String fromAddress = user + "@tej.com.tw";
	private static String[] recipients = {  };

	public void setMailReciver(String[] Reciver) {
		recipients = Reciver;
	}

	public void setMailReciver(String Reciver) {
		recipients = new String[1];
		recipients[0] = Reciver;
	}
	
	public void setMailAccount(String user,String passwd) {
		this.user=user;
		this.passwd=passwd;
	}
	
	/*
	 * 使用TEJ 內部的MAIL SERVER 寄信
	 * subString:信件的Title
	 * message:內文部分
	 */
	public void sendMail(String subString, String message)
			throws MessagingException {
		boolean debug = false;
		Properties prop = new Properties();
		System.setProperty("mail.mime.charset", "BIG5");

		// set the Parameters to prop
		prop.put("mail.smtp.host", SMTP_HOST_NAME);
		//235機器要多認證localhost
		prop.put("mail.smtp.localhost", "97.24.3.235"); 
		prop.put("mail.smtp.auth", "true");
		prop.put("mail.debug", debug);
		prop.put("mail.smtp.port", SMTP_PORT);
		prop.put("mail.smtp.socketFacotory.port", SMTP_PORT);
		// prop.put("mail.smtp.socketFactory.class", SSL_FACTORY);
		prop.put("mail.smtp.cocketFactory.fallback", "false");

		Session mailsSession = Session.getInstance(prop, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user, passwd);
			}
		});
		mailsSession.setDebug(debug);
		Message msg = new MimeMessage(mailsSession);
		// Address FromEmail=new InternetAddress(fromAddress);

		InternetAddress FromEmail = new InternetAddress(fromAddress);
		msg.setFrom(FromEmail);

		InternetAddress[] ToEmail = new InternetAddress[recipients.length];
		for (int i = 0; i < recipients.length; i++) {
			ToEmail[i] = new InternetAddress(recipients[i]);
		}

		// to many address use setRecipients

		msg.setRecipients(Message.RecipientType.TO, ToEmail);
		msg.setSubject(subString);
		msg.setContent(message, "text/html; charset=BIG5");
		Transport.send(msg);


	}

	public static String[] getStrings(String oldstr, String sym) {
		String[] a = null;
		StringTokenizer st = new StringTokenizer(oldstr, sym);
		int n = st.countTokens();
		a = new String[n];
		int i = 0;
		while (st.hasMoreTokens()) {
			a[i] = st.nextToken();
			i++;
		}
		return a;
	}
}
