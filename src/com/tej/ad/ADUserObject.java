package com.tej.ad;
import javax.naming.directory.Attribute;

public class ADUserObject {

	private String user_accout;
	private String user_mail;
	private String user_depat;
	private String user_name;
	
	ADUserObject(Attribute accout,Attribute mail,Attribute dep,Attribute name){
//		if(accout!=null&&mail!=null&&mail!=null){
			this.user_accout=convertAttribute(accout);
			this.user_mail=convertAttribute(mail);
			this.user_depat=convertAttribute(dep);
			this.user_name=convertAttribute(name);
//		}
		
	}
	
	public String getName(){
		return user_name;
	}
	
	public String getDepartment(){
		return user_depat;
	}
	
	public String getMail(){
		return user_mail;
	}
	
	public String getID(){
		return user_accout;
	}
	
	/*
	 * 將Attribute 轉成string 回傳
	 */
    public String convertAttribute(Attribute attr){
    	if(attr!=null){
    		String result=attr.toString().split(":")[1].trim();
    		if(result.indexOf("-")!=-1){
    			result=result.split("-")[1];
    		}
    		return result;
    	}else{
    		return "";
    	}
    	
    }
	

}
