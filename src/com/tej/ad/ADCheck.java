package com.tej.ad;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Properties;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import tw.com.tej.tools.RequestBODY;
import tw.com.tej.tools.httpTools;

/*
 * 處理Login 時帳號認證的處理
 */

@Path("/login")
public class ADCheck {
//	private Logger log = Logger.getLogger("rfp");
//	@POST
//	@Path("/check")
//	@Consumes("application/json")
//	@Produces(MediaType.APPLICATION_JSON)
//	public String requestVersionAndTicket(@Context HttpServletRequest request,
//			@Context HttpServletResponse res, InputStream requestBodyStream)
//			throws JSONException, UnsupportedEncodingException, NamingException {
//
//		JSONObject requestJSON = new JSONObject(RequestBODY.get(
//				requestBodyStream, Charset.forName("utf-8")));
//		String username = requestJSON.getString("user");
//		String passwd = requestJSON.getString("passwd");
//		JSONObject respJSON = new JSONObject();
//		ADAuth adAuth = new ADAuth(username, passwd);
//		boolean check = adAuth.checkLogin();
//		if (check) {
//			respJSON.put("status", 200);
//			String sessionid=generateSessionId();
//			HttpSession session = request.getSession();
//			session.setAttribute("sessionid", sessionid);
//			session.setAttribute("user", changeUsernameForMis(username));
//			log.info("User:"+changeUsernameForMis(username)+" login success");
//		} else {
//			respJSON.put("status", 404);
//			//log.info("User:"+changeUsernameForMis(username)+" login Fail");
//		}
//		return respJSON.toString();
//	}

	private static String generateSessionId()
			throws UnsupportedEncodingException {
		String uid = new java.rmi.server.UID().toString(); // guaranteed unique
		return URLEncoder.encode(uid, "UTF-8"); // encode any special chars
	}
	
	
	@GET
	@Path("/requesttoken")
	@Consumes("application/json")
	@Produces(MediaType.TEXT_PLAIN)
	public String requestVersionAndTicket2(@Context HttpServletRequest request,
			@Context HttpServletResponse res, InputStream requestBodyStream)
			throws JSONException, UnsupportedEncodingException, NamingException {
		String oauth_token=request.getParameter("oauth_token");
		String oauth_verifier=request.getParameter("oauth_verifier");
		if(!org.apache.commons.lang3.StringUtils.isEmpty(oauth_token)&&!StringUtils.isEmpty(oauth_verifier)){
			request.getSession().setAttribute("oauth_token", oauth_token);
			request.getSession().setAttribute("oauth_token", oauth_verifier);
		}
		//request.getParameter("oauth_token")?"":request.getParameter("oauth_token");
		String yahoo_request_token_url="https://api.login.yahoo.com/oauth/v2/get_request_token";
		System.out.println("oauth_token:"+oauth_token+" oauth_verifier:"+oauth_verifier);
		JSONObject respJSON = new JSONObject();
		String oauth_token_secret="fe9ff1f62969ac4b25354adc3b3feef335573097%26";
		String oauth_consumer_key="dj0yJmk9RUhuVHhLWjRGUm1nJmQ9WVdrOU0yRmFabTA0Tm1zbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD02Ng--";
		HashMap<String,  String> yahoo_map = new HashMap<String, String>();
		java.util.Date date=new java.util.Date();
		long timeLong = date.getTime() / 1000;
		yahoo_map.put("oauth_consumer_key", oauth_consumer_key);
        yahoo_map.put("oauth_timestamp", String
        		.valueOf(timeLong));
        //yahoo_map.put("oauth_signature",oauth_token+ oauth_token_secret);
        yahoo_map.put("oauth_signature",oauth_token_secret);
        yahoo_map.put("oauth_verifier", oauth_verifier);
        yahoo_map.put("oauth_signature_method", "PLAINTEXT");
        yahoo_map.put("oauth_nonce", "e43f20fe645b5d674f2c71e04289d2b05");
        yahoo_map.put("oauth_version", "1.0");
        yahoo_map.put("oauth_token", oauth_token);
        yahoo_map.put("oauth_callback", "http://kofman.twbbs.org/"
			+ "restWeb/services/login/gettoken");
        httpTools tools=new httpTools();
        String result=tools.excePost(yahoo_request_token_url, yahoo_map);
        
	result = result.replaceAll("&", "\n");
	           System.out.println(result);
	           //new ByteArrayInputStream(result.getBytes());
	           Properties pro = new Properties();

	              try {
					pro.load(new ByteArrayInputStream(result.getBytes()));
					if(!StringUtils.isEmpty((CharSequence) pro.get("oauth_token_secret"))){
						System.out.println("==xoauth_request_auth_url Start===");
						String urlString=URLDecoder.decode((String) pro.get("xoauth_request_auth_url"));
						//System.out.println("url:"+urlString);
						//System.out.println("excuteGET:"+tools.excuteGET(urlString));
						request.getSession().setAttribute("oauth_token_secret",pro.get("oauth_token_secret"));
						res.sendRedirect(urlString);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	              
		System.out.println(result);
		//getToken(request, oauth_token, oauth_verifier, oauth_token_secret_1)

		//result=getToken(request,oauth_token, oauth_verifier,(String)pro.get("oauth_token_secret"));
	
		return result;
}
	
	
	@GET
	@Path("/gettoken")
	@Consumes("application/json")
	@Produces(MediaType.TEXT_PLAIN)
	public String requestVersionAndTicket3(@Context HttpServletRequest request,
			@Context HttpServletResponse res, InputStream requestBodyStream)
			throws JSONException, UnsupportedEncodingException, NamingException {
		System.out.println("======gettoken Start=======");
		String yahoo_get_token_url="https://api.login.yahoo.com/oauth/v2/get_token";
		String oauth_token=request.getParameter("oauth_token");
		String oauth_verifier=request.getParameter("oauth_verifier");
		String oauth_token_secret_1=(String) request.getSession().getAttribute("oauth_token_secret");
		System.out.println("oauth_token:"+oauth_token+" oauth_verifier:"+oauth_verifier+" oauth_token_secret_1:"+oauth_token_secret_1);
		JSONObject respJSON = new JSONObject();
		String oauth_token_secret="fe9ff1f62969ac4b25354adc3b3feef335573097%26"+oauth_token_secret_1;
		String oauth_consumer_key="dj0yJmk9RUhuVHhLWjRGUm1nJmQ9WVdrOU0yRmFabTA0Tm1zbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD02Ng--";
		HashMap<String,  String> yahoo_map = new HashMap<String, String>();
		java.util.Date date=new java.util.Date();
		long timeLong = date.getTime() / 1000;
		yahoo_map.put("oauth_consumer_key", oauth_consumer_key);
        yahoo_map.put("oauth_timestamp", String
        		.valueOf(timeLong));
        //yahoo_map.put("oauth_signature",oauth_token+ oauth_token_secret);
        yahoo_map.put("oauth_signature",oauth_token_secret);
        yahoo_map.put("oauth_verifier", oauth_verifier);
        yahoo_map.put("oauth_signature_method", "PLAINTEXT");
        yahoo_map.put("oauth_nonce", "e43f20fe645b5d674f2c71e04289d2b05");
        yahoo_map.put("oauth_version", "1.0");
        yahoo_map.put("oauth_token", oauth_token);
        //yahoo_map.put("oauth_callback", "http://kofman.twbbs.org/restWeb/services/login/test2");
        httpTools tools=new httpTools();
        String result=tools.excePost(yahoo_get_token_url, yahoo_map);
        
        result = result.replaceAll("&", "\n");
	    System.out.println(result);
	    
	    result = result.replaceAll("&", "\n");
        System.out.println(result);
        //new ByteArrayInputStream(result.getBytes());
        Properties pro = new Properties();

           try {
				pro.load(new ByteArrayInputStream(result.getBytes()));
				if(!StringUtils.isEmpty((CharSequence) pro.get("xoauth_yahoo_guid"))){
					System.out.println("==xoauth_request_auth_url Start===");
					
					String xoauth_yahoo_guid=(String) pro.get("xoauth_yahoo_guid");
					String token=(String) pro.get("oauth_token");
					String token_secret=(String) pro.get("oauth_token_secret");
					System.out.println(xoauth_yahoo_guid+" : "+token+"  : "+token_secret);
					respJSON.put("xoauth_yahoo_guid", xoauth_yahoo_guid);
					respJSON.put("oauth_token", token);
					respJSON.put("oauth_token_secret", token_secret);
					HashMap<String, String> map=new HashMap<String, String>();
					String urlString="https://social.yahooapis.com/v1/user/" + xoauth_yahoo_guid+ "/profile/profile/usercard";
					timeLong = date.getTime() / 1000;
					yahoo_map.put("realm","yahooapis.com");
			        yahoo_map.put("formate", "json");
					yahoo_map.put("oauth_timestamp", String
			        		.valueOf(timeLong));
			        //yahoo_map.put("oauth_token", oauth_token);
			        yahoo_map.put("oauth_signature", token_secret+"%26");
			        yahoo_map.put("oauth_version","1.0");
			        map.put("realm","yahooapis.com");
					timeLong = date.getTime() / 1000;
					String oauth_token_secret_2="fe9ff1f62969ac4b25354adc3b3feef335573097%26";
					String oauth_consumer_key_2="dj0yJmk9RUhuVHhLWjRGUm1nJmQ9WVdrOU0yRmFabTA0Tm1zbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD02Ng--";
					 OAuthConsumer consumer = new DefaultOAuthConsumer(oauth_consumer_key_2, oauth_token_secret_2);
					    URL url = new URL("https://social.yahooapis.com/v1/user/"+xoauth_yahoo_guid+"/contacts?format=json");
					    HttpURLConnection request1 = (HttpURLConnection) url.openConnection();
					    consumer.setTokenWithSecret(token,token_secret+"%26");
					    System.out.println("TOKEN:"+token);
					    System.out.println("token_secret:"+token_secret);
					    System.out.println("oauth_token_secret_2:"+oauth_token_secret_2);
					    System.out.println("oauth_consumer_key_2:"+oauth_consumer_key_2);
					    consumer.sign(request1);
					    request1.connect();
					    String responseBody = convertStreamToString(request1.getInputStream());
					    System.out.println(responseBody);
//					map.put("realm","yahooapis.com");
//			        map.put("formate", "json");
//					map.put("oauth_timestamp", String
//			        		.valueOf(timeLong));
//			        map.put("oauth_token", oauth_token);
//			        map.put("oauth_signature", token_secret+"%26");
//			        map.put("oauth_version","1.0");
			        //urlString="https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20social.profile%20where%20guid%20%3D%20me&format=json&diagnostics=true&callback=";
			        
			        //res.sendRedirect(urlString);
			        //getUserInfo(xoauth_yahoo_guid);
			        //System.out.println("excuteGET:"+tools.exceGet(urlString,yahoo_map));
					//System.out.println("excutePOST:"+tools.excePost(urlString, yahoo_map));
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (OAuthMessageSignerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (OAuthExpectationFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (OAuthCommunicationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	   // xoauth_yahoo_guid
	    System.out.println("======gettoken DONE=======");
		return respJSON.toString();
	}
	
    public static String convertStreamToString(InputStream is) throws UnsupportedEncodingException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"utf-8"));
    StringBuilder sb = new StringBuilder();
    String line = null;
    try {
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
    } catch (IOException e) {
    } finally {
        try {
            is.close();
        } catch (IOException e) {
        }
    }
    return sb.toString();
}
	
	public void getUserInfo(String guid){
		String baseUrl = "http://query.yahooapis.com/v1/public/yql?q=";
		String query = "select * from social.profile where guid=me&format=json";
		try {
//			String url="http://social.yahooapis.com/v1/user/"+guid+"/profile?format=json&realm=yahooapis.com&oauth_consumer_key="+CONSUMER_KEY+"&"
//					+ "oauth_signature_method=HMAC-SHA1&oauth_version=1.0&oauth_timestamp="
//		    +profileTimeStamp+"&oauth_nonce=24829869585&oauth_token="+oauthTokenRefresh+"&oauth_signature="+signatureProfile;
			String fullUrlStr = baseUrl + URLEncoder.encode(query, "UTF-8") + "&format=json";

			URL fullUrl = new URL(fullUrlStr);
			InputStream is = fullUrl.openStream();

			JSONTokener tok = new JSONTokener(is);
			JSONObject result = new JSONObject(tok);

			is.close();
			System.out.println(result);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//String url="http://social.yahooapis.com/v1/user/"+abcdef123+"/profile?format=json"; 
	}
	
	
	public String getToken(HttpServletRequest request,String oauth_token,String oauth_verifier,String oauth_token_secret_1){
		System.out.println("======gettoken Start=======");
		String yahoo_get_token_url="https://api.login.yahoo.com/oauth/v2/get_token";
		//String oauth_token=(String) request.getSession().getAttribute("oauth_token");
		//String oauth_verifier=(String) request.getSession().getAttribute("oauth_verifier");
		//String oauth_token_secret_1=(String) request.getSession().getAttribute("oauth_token_secret");
		System.out.println("oauth_token:"+oauth_token+" oauth_verifier:"+oauth_verifier+" oauth_token_secret_1:"+oauth_token_secret_1);
		JSONObject respJSON = new JSONObject();
		String oauth_signature="fe9ff1f62969ac4b25354adc3b3feef335573097%26"+oauth_token_secret_1;
		String oauth_consumer_key="dj0yJmk9RUhuVHhLWjRGUm1nJmQ9WVdrOU0yRmFabTA0Tm1zbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD02Ng--";
		HashMap<String,  String> yahoo_map = new HashMap<String, String>();
		java.util.Date date=new java.util.Date();
		long timeLong = date.getTime() / 1000;
		yahoo_map.put("oauth_consumer_key", oauth_consumer_key);
        yahoo_map.put("oauth_timestamp", String
        		.valueOf(timeLong));
        //yahoo_map.put("oauth_signature",oauth_token+ oauth_token_secret);
        yahoo_map.put("oauth_signature",oauth_signature);
        yahoo_map.put("oauth_verifier", oauth_verifier);
        yahoo_map.put("oauth_signature_method", "PLAINTEXT");
        yahoo_map.put("oauth_nonce", "e43f20fe645b5d674f2c71e04289d2b05");
        yahoo_map.put("oauth_version", "1.0");
        yahoo_map.put("oauth_token", oauth_token);
        //yahoo_map.put("oauth_callback", "http://kofman.twbbs.org/restWeb/services/login/test2");
        httpTools tools=new httpTools();
        String result=tools.excePost(yahoo_get_token_url, yahoo_map);
        
        result = result.replaceAll("&", "\n");
	    System.out.println(result);
	    System.out.println("======gettoken DONE=======");
		return respJSON.toString();
	}
	

	/*
	 * oauth_token=d66avxk
oauth_token_secret=e51ea0ad5bc47104dc117c668c1fc2937dc73b7f
oauth_expires_in=3600
xoauth_request_auth_url=https%3A%2F%2Fapi.login.yahoo.com%2Foauth%2Fv2%2Frequest_auth%3Foauth_token%3Dd66avxk
oauth_callback_confirmed=true

	 */
	//轉換AD User跟MIS部分User不同帳號的問題(尚缺調查)
//	private String changeUsernameForMis(String user){
////		String name=user.toLowerCase();
////		switch(user){
////		case "jane.lee":
////			name="lsj";
////			break;
////		default:
////			break;
////		}
////		return name;
////		
//	}

}
