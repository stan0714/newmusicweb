package com.tej.ad;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import com.tej.Properties.PropertiesCenter;

public class ADAuth {
	private String logonName = "";
	//ArrayList<ADUserObject> userlist = new ArrayList<>(0);
	private String logonPassWord = "";

	private String adServerIp = "97.24.2.201";
	private String domain = "@tejwin.com";
	private String adServerPort = "389";
	
	
	public void setADServerInfo(){
		adServerIp=PropertiesCenter.getPropertiesValue("adServerIp");
		domain=PropertiesCenter.getPropertiesValue("addomain");
		adServerPort=PropertiesCenter.getPropertiesValue("adServerPort");
	}
	
	private String adRoot = "dc=tejwin , dc=com ";

	private DirContext dirContext;
	
	public volatile static ADAuth utillong = null;

	public ADAuth(String username, String passwd) throws NamingException,
			UnsupportedEncodingException {
		setADServerInfo();
		this.logonName = username;
		this.logonPassWord = passwd;
		
	}

	public ADAuth() throws NamingException, UnsupportedEncodingException {

		this.dirContext = this.createConnection();
		// search();
		//serachControl();
		dirContext.close();

	}

	public synchronized boolean checkLogin() {

		boolean gate = false;
		Hashtable env = new Hashtable();

		env.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");

		env.put(Context.PROVIDER_URL, "ldap://" + adServerIp + ":"
				+ adServerPort + "/");

		env.put(Context.SECURITY_AUTHENTICATION, "simple");

		env.put(Context.SECURITY_PRINCIPAL, logonName + domain);
		// env.put(Context.SECURITY_PRINCIPAL, "cn="+logonName+","+adRoot);
		// env.put(Context.SECURITY_PRINCIPAL, "cn="+logonName+"," +
		// "ou=it,ou=shanghai" + adRoot);

		env.put(Context.SECURITY_CREDENTIALS, logonPassWord);

		try {

			this.dirContext = new InitialDirContext(env);

			gate = true;

		} catch (javax.naming.AuthenticationException e) {

			System.out.println("Fail " + e.toString());

		} catch (Exception e) {

			System.out.println("Fail�G" + e.toString());

		}

		return gate;
	}
    
	public void search() throws NamingException, UnsupportedEncodingException {
		Attributes allAttr = dirContext.getAttributes(adRoot); // get *a*
																// object's all
																// attributes
		for (NamingEnumeration attrs = allAttr.getAll(); attrs.hasMore();) {
			Attribute attr = (Attribute) attrs.next();
			System.out.println("Attribute is : " + attr.getID());
			for (NamingEnumeration values = attr.getAll(); values.hasMore();) {
				String val = (String) values.next();
				System.out.println("    value is : "
						+ new String(val.getBytes("iso-8859-1"), "UTF-8"));
			}
		}

	}

//	public void serachControl() throws NamingException {
//		SearchControls searchCtls = new SearchControls();
//
//		// Specify the attributes to return
//		String returnedAtts[] = { "sn", "givenName", "samAccountName", "mail",
//				"department", "name", "displayName", "mailNickname " };
//		searchCtls.setReturningAttributes(returnedAtts);
//
//		// Specify the search scope
//		searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
//
//		// specify the LDAP search filter
//		String searchFilter = "(&(objectClass=user))";
//
//		// Specify the Base for the search
//		// String searchBase = "dc=dom,dc=fr";
//		// initialize counter to total the results
//		int totalResults = 0;
//
//		// Search for objects using the filter
//		NamingEnumeration<SearchResult> answer = dirContext.search(adRoot,
//				searchFilter, searchCtls);
//		// Loop through the search results
//		while (answer.hasMoreElements()) {
//			SearchResult sr = (SearchResult) answer.next();
//
//			totalResults++;
//
//			Attributes attrs = sr.getAttributes();
//
//			Attribute accout = attrs.get("samAccountName");
//			Attribute mail = attrs.get("mail");
//			Attribute name = attrs.get("name");
//			Attribute dep = attrs.get("department");
//			if (accout != null && mail != null && name != null) {
//				ADUserObject adUserObject = new ADUserObject(accout, mail, dep,
//						name);
//				userlist.add(adUserObject);
//			}
//		}
//		for (int i = 0; i < userlist.size(); i++) {
//			ADUserObject adUserObject = userlist.get(i);
//
//			System.out.println(adUserObject.getID() + " user:"
//					+ adUserObject.getName() + " "
//					+ adUserObject.getDepartment());
//		}
//	}

	public static synchronized ADAuth getInstance() throws NamingException,
			UnsupportedEncodingException {

		if (utillong == null) {

			utillong = new ADAuth();

		}

		return utillong;

	}

	private synchronized DirContext createConnection() {

		Hashtable env = new Hashtable();

		env.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");

		env.put(Context.PROVIDER_URL, "ldap://" + adServerIp + ":"
				+ adServerPort + "/");

		env.put(Context.SECURITY_AUTHENTICATION, "simple");

		env.put(Context.SECURITY_PRINCIPAL, logonName + domain);
		// env.put(Context.SECURITY_PRINCIPAL, "cn="+logonName+","+adRoot);
		// env.put(Context.SECURITY_PRINCIPAL, "cn="+logonName+"," +
		// "ou=it,ou=shanghai" + adRoot);

		env.put(Context.SECURITY_CREDENTIALS, logonPassWord);

		try {

			this.dirContext = new InitialDirContext(env);

			System.out.println("Success");

		} catch (javax.naming.AuthenticationException e) {

			System.out.println("Fail " + e.toString());

		} catch (Exception e) {

			System.out.println("Fail�G" + e.toString());

		}

		return this.dirContext;

	}

	public synchronized NamingEnumeration search(String filter)
			throws NamingException {

		if (!isConn()) {

			this.reConn();

		}

		SearchControls searchControls = new SearchControls();

		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		return this.dirContext.search(adRoot, filter, searchControls);

	}

	public synchronized NamingEnumeration search(String base, String filter)
			throws NamingException {

		if (!base.toLowerCase().endsWith(adRoot.toLowerCase())) {

			base = base + "," + adRoot;

		}

		SearchControls searchControls = new SearchControls();

		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

		return this.dirContext.search(base, filter, searchControls);

	}

	public boolean isConn() throws NamingException {

		SearchControls searchControls = new SearchControls();

		NamingEnumeration answer = this.dirContext.search(adRoot,
				"(objectclass={0})", new Object[] { "top".getBytes() },
				searchControls);

		boolean flag = answer.hasMore();

		return flag;

	}

	public synchronized boolean reConn() {

		System.out.println("reConnect");

		boolean flag = false;

		try {

			this.dirContext = this.createConnection();

			flag = true;

		} catch (RuntimeException e) {

			flag = false;

		}

		return flag;

	}

	public synchronized void close() {

		if (this.dirContext != null) {

			try {

				this.dirContext.close();

				utillong = null;

			} catch (NamingException e) {

				System.out.println("NamingException in close():" + e);

			}

		}

	}
	
	public static void main(String[] args) {
		try {
			ADAuth adAuth = new ADAuth();
		} catch (NamingException e) {

		} catch (UnsupportedEncodingException e) {
			// TODO: handle exception
		}

	}

}
