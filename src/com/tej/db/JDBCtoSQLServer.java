package com.tej.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import com.tej.person.employee_info;

public class JDBCtoSQLServer {

	/**
	 * @param args
	 */
	int port = 1433;
	String server = "tej-express";
	String driverName = "com.microsoft.sqlserver.jdbc.SQLServerDriver"; // JDBC TO SQL Server
	String dbURL = "jdbc:sqlserver://tej-express:1433;";
	String userName = "sa"; // 如果沒丟特殊使用者 所使用的使用者參數名稱
	String userPwd = "tejvar"; // 密碼
	static Connection connection = null;

	public JDBCtoSQLServer() throws ClassNotFoundException, SQLException {
		createConnect();

	}

	public JDBCtoSQLServer(String server, int port, String username,String passwd)
			throws ClassNotFoundException, SQLException {
		this.server = server;
		this.port = port;
		this.userPwd=passwd;
		this.userName=username;
		createConnect();
	}

	public void createConnect() throws ClassNotFoundException, SQLException {
		if (connection == null) {
			dbURL = "jdbc:sqlserver://" + server + ":" + port;
			Class.forName(driverName);
			connection = DriverManager.getConnection(dbURL, userName, userPwd);
			System.out.println("Connection create Successful! ");
		}
	}

	public void exitConnect() throws SQLException {
		if (connection != null) {
			System.out.println("Connection exit Successful! ");
			connection.close();
		}
	}
		
	public HashMap<String, ArrayList<employee_info>> getAllUserAtMIS() throws SQLException {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		HashMap<String, ArrayList<employee_info>> resultMap = new HashMap<String, ArrayList<employee_info>>();
		String SQL = "select db1.UserChineseName, db1.UserName,db1.Email,db2.MajorCategories,db1.OnTheJob from SecurityDB.dbo.aspnet_Users db1 join TEJMIS.dbo.Department db2 on db1.Dept = db2.ID WHERE db1.Email <> '' and db1.Email is not null and db1.OnTheJob = 1 order by db2.MajorCategories ";
		stmt = connection.prepareStatement(SQL);
		rs = stmt.executeQuery();
		while (rs.next()) {
			String dep = rs.getString(4);
			employee_info emyInfo = new employee_info(rs.getString(2),
					rs.getString(1), rs.getString(3),rs.getString(4));

			if (resultMap.containsKey(dep)) {
				ArrayList<employee_info> listTemp = resultMap.get(dep);
				listTemp.add(emyInfo);
				resultMap.put(dep, listTemp);
			} else {
				ArrayList<employee_info> listTemp = new ArrayList<employee_info>();
				listTemp.add(emyInfo);
				resultMap.put(dep, listTemp);
			}
		}
		return resultMap;
	}

	public static void main(String[] args) {
		
			JDBCtoSQLServer jdbCtoSQLServe;
			try {
				jdbCtoSQLServe = new JDBCtoSQLServer();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			jdbCtoSQLServe.getAllUserAtMIS();
//			jdbCtoSQLServe.exitConnect();
		


	}

}
