package com.tej.db;

import java.io.UnsupportedEncodingException;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;

import tw.com.tej.tools.TimeTools;
import com.tej.person.employee_info;
import com.tej.rfp.UnfinishObject;

public class JDBCtoPostgres {

	String connect_address = "97.24.3.232";
	String connect_port = "5444";
	String connect_user = "enterprisedb";
	String connect_passwd = "postgres";
	String connect_db = "tw_test";
	static Connection connection = null;

	public JDBCtoPostgres(String serveraddress, String port, String username,
			String passwd, String dbname) {
		this.connect_address = serveraddress;
		this.connect_port = port;
		this.connect_passwd = passwd;
		this.connect_user = username;
		this.connect_db = dbname;
		setConnect();

	}

	public JDBCtoPostgres() {
		setConnect();

	}

	public void setConnect() {
		try {

			Class.forName("org.postgresql.Driver");

		} catch (ClassNotFoundException e) {

			e.printStackTrace();

		}

		try {

			connection = DriverManager.getConnection("jdbc:postgresql://"
					+ connect_address + ":" + connect_port + "/" + connect_db,
					connect_user, connect_passwd);

		} catch (SQLException e) {

			e.printStackTrace();
			return;

		}

	}

	public void exitSQLConnect() {
		if (connection != null) {
			try {
				connection.close();
				connection = null;
			} catch (SQLException e) {

			}
		}
	}

	// 動態從資料庫選取
	public ArrayList<String> GETRfpNumberForAutoCom() {
		ArrayList<String> resultList = new ArrayList<String>();

		Statement stmt = null;
		ResultSet rs = null;

		String SQL = "select work_number_id from public.work_number_profile";
		try {

			stmt = connection.createStatement();
			rs = stmt.executeQuery(SQL);
			connection.close();
			while (rs.next()) {
				resultList.add(rs.getString(1));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {

				rs.close();
				stmt.close();
			} catch (SQLException e) {

			}
		}
		return resultList;

	}

	// 得到TEJ所有員工的部門 跟部門下的人員
	public HashMap<String, ArrayList<UnfinishObject>> checkUnfinishRFP(String date)
			throws SQLException {
		HashMap<String, ArrayList<UnfinishObject>> resultMap = new HashMap<String,ArrayList<UnfinishObject>>();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String SQL = "select t1.work_number_id,t2.estimate_date, t1.status,  t2.is_question, t1.correspond_person_id, t3.employee_name, t3.employee_mail ,t2.hope_finish_date"
				+" from  (( public.work_employee_mapping t1 join public.work_number_profile t2  on t1.work_number_id= t2.work_number_id ) left join public.employee_work_profile t3"
				+ " ON t3.employee_id=t1.correspond_person_id )  where  t1.finish_date is NULL and ((t2.estimate_date < '"+date+"' and t2.hope_finish_date is NULL ) or ( t2.hope_finish_date <'"+date+"' ) )";
		stmt = connection.prepareStatement(SQL);
		rs = stmt.executeQuery();

		while (rs.next()) {
			String id = rs.getString(5);
			String name=rs.getString(6);
			String mail=rs.getString(7);
			String person=id+"-"+name+"-"+mail;
			UnfinishObject jobInfo = new UnfinishObject(rs.getString(1),
					rs.getString(2),rs.getString(3) ,rs.getBoolean(4),rs.getString(8));
			
			if (resultMap.containsKey(person)) {
				ArrayList<UnfinishObject> listTemp = resultMap.get(person);
				listTemp.add(jobInfo);
				resultMap.put(person, listTemp);
			} else {
				ArrayList<UnfinishObject> listTemp = new ArrayList<UnfinishObject>();
				listTemp.add(jobInfo);
				resultMap.put(person, listTemp);
			}

		}
		rs.close();
		stmt.close();
		if (connection != null) {
			connection.close();
		}

		return resultMap;

	}
	
	
	// 得到TEJ所有員工的部門 跟部門下的人員
	public HashMap<String, ArrayList<employee_info>> getUserlist()
			throws SQLException {
		HashMap<String, ArrayList<employee_info>> resultMap = new HashMap<String, ArrayList<employee_info>>();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String SQL = "SELECT  employee_work_profile.employee_department,employee_work_profile.employee_id, employee_work_profile.employee_name,employee_work_profile.employee_mail,employee_work_profile.onthejob "
				+ "FROM employee_work_profile where employee_work_profile.onthejob='1'"
				+ "order by (employee_work_profile.employee_department,employee_work_profile.employee_id) desc ";

		stmt = connection.prepareStatement(SQL);

		rs = stmt.executeQuery();

		while (rs.next()) {
			String dep = rs.getString(1);

			employee_info emyInfo = new employee_info(rs.getString(2),
					rs.getString(3), rs.getString(4),rs.getString(5));
			if (dep.equals("")) {
				dep = "其他";
			}
			if (resultMap.containsKey(dep)) {
				ArrayList<employee_info> listTemp = resultMap.get(dep);
				listTemp.add(emyInfo);
				resultMap.put(dep, listTemp);
			} else {
				ArrayList<employee_info> listTemp = new ArrayList<employee_info>();
				listTemp.add(emyInfo);
				resultMap.put(dep, listTemp);
			}

		}

		rs.close();
		stmt.close();
		if (connection != null) {
			connection.close();
		}

		return resultMap;

	}


	// 檢時傳輸number是否已經有存在
	public boolean checkRfpNumber(String number) {
		boolean result = false;
		PreparedStatement stmt = null;
		String SQL = "select work_number_id from work_number_profile where work_number_id=?";

		try {
			stmt = connection.prepareStatement(SQL);
			stmt.setString(1, number);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				result = true;
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;

	}
	// 得到RFPNumber的相關資訊
	public ArrayList<String> getRfpNumberInfo(String number) {
		ArrayList<String> resultList = new ArrayList<String>();
		PreparedStatement stmt = null;
		String SQL = "select work_number_description, estimate_date,estimate_hours,work_number_id,hope_finish_date from work_number_profile where work_number_id=?";

		try {
			stmt = connection.prepareStatement(SQL);
			stmt.setString(1, number);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				for (int i = 1; i < 6; i++) {
					resultList.add(rs.getString(i));
				}

			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return resultList;

	}

	/*
	 * 新增一筆rfp記錄 number:RFP編號單ID type:是否為需求單(false為問題單) contect:工作單說明
	 * date:預計完成日期 hours:預計完成工作時數
	 */
	public void insertNewRfpNumber(String number, boolean type, String content,
			String date, int hours) throws ParseException {
		PreparedStatement stmt = null;
		String SQL = "insert into work_number_profile (work_number_id,is_question, work_number_description,estimate_date,estimate_hours) VALUES (?,?,?,?,?)";
		try {
			stmt = connection.prepareStatement(SQL);
			stmt.setString(1, number);
			stmt.setBoolean(2, type);
			stmt.setString(3, content);

			stmt.setTimestamp(4, TimeTools.convertStringtoTimestamp(date));
			stmt.setInt(5, hours);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * 更新rfp記錄 number:RFP編號單ID od:od編號 contect:備註 date:完成日期 hours:完成工作時數
	 */
	public void updateRfpNumber(String number, int od, String user,
			String content, String startdate, String enddate, int hours)
			throws ParseException {
		PreparedStatement stmt = null;
		String SQL = "update  work_employee_mapping SET record_person_id=?,start_date=?,finish_date=?,real_hours=?,comment=? where work_number_id=? and work_number_od=?";
		try {
			stmt = connection.prepareStatement(SQL);
			stmt.setString(1, user);
			stmt.setTimestamp(2, TimeTools.convertStringtoTimestamp(startdate));
			stmt.setTimestamp(3, TimeTools.convertStringtoTimestamp(enddate));
			stmt.setInt(4, hours);
			stmt.setString(5, content);
			stmt.setString(6, number);
			stmt.setInt(7, od);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * 更新rfp記錄 number:RFP編號單ID od:od編號 contect:備註 date:完成日期 hours:完成工作時數
	 */
	public void updateCommentbyRfpNumber(String number, int od,
			String content)
			throws ParseException {
		PreparedStatement stmt = null;
		String SQL = "update  work_employee_mapping SET comment=? where work_number_id=? and work_number_od=?";
		try {
			stmt = connection.prepareStatement(SQL);
			stmt.setString(1, content);
			stmt.setString(2, number);
			stmt.setInt(3, od);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	/*
	 * 得到OD編號的最大筆 number:RFP編號單ID
	 */
	public int getMaxODNumber(String number) {
		PreparedStatement stmt = null;
		String SQL = "select MAX(work_number_od) from work_employee_mapping where work_number_id=? ";
		int result = -1;
		try {
			stmt = connection.prepareStatement(SQL);
			stmt.setString(1, number);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				result = rs.getInt(1);
			}
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	/*
	 * 完成一筆rfp記錄 number:RFP編號單ID contect:工作單說明 user:登入使用者 date:預計完成日期
	 * hours:完成工作時數
	 */
	public void completeRfpNumber(String number, String user, String status,
			String content, String date, int hours,int before_od) throws JSONException,
			ParseException {
		PreparedStatement stmt = null;
		String SQL = "insert into work_employee_mapping (work_number_id,work_number_od,status, apply_person_id,correspond_person_id,record_person_id,finish_date,real_hours,before_od) VALUES (?,?,?,?,?,?,?,?,?)";

		try {
			int odnumber = getMaxODNumber(number);
			odnumber++;
			stmt = connection.prepareStatement(SQL);
			stmt.setString(1, number);
			stmt.setInt(2, odnumber);
			stmt.setString(3, status);
			stmt.setString(4, user);
			stmt.setString(5, user);
			stmt.setString(6, user);
			stmt.setTimestamp(7, TimeTools.convertStringtoTimestamp(date));
			stmt.setInt(8, hours);
			stmt.setInt(9, before_od);
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * 新增一筆rfp記錄 number:RFP編號單ID type:是否為需求單(false為問題單) contect:工作單說明
	 * date:預計完成日期 hours:預計完成工作時數,before_od:先前的OD欄位
	 */
	public void inserRfpNumber(String number, String user, JSONArray person,
			int od, String status,int berfor_od) throws JSONException {
		PreparedStatement stmt = null;
		String SQL = "insert into work_employee_mapping (work_number_id,work_number_od,status, apply_person_id,correspond_person_id,before_od) VALUES (?,?,?,?,?,?)";

		try {
			int odnumber = getMaxODNumber(number);
			if (person.length() == 1) {
				odnumber++;
				stmt = connection.prepareStatement(SQL);
				stmt.setString(1, number);
				stmt.setInt(2, odnumber);
				stmt.setString(3, status);
				stmt.setString(4, user);
				stmt.setString(5, person.getString(0).split("_")[0]);
				stmt.setInt(6, berfor_od);
				stmt.executeUpdate();
			} else {
				for (int i = 0; i < person.length(); i++) {
					odnumber = odnumber + 1;
					stmt = connection.prepareStatement(SQL);
					stmt.setString(1, number);
					stmt.setInt(2, odnumber);
					stmt.setString(3, status);
					stmt.setString(4, user);
					stmt.setString(5, person.getString(i).split("_")[0]);
					stmt.setInt(6, berfor_od);
					stmt.executeUpdate();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	// 動態從資料庫選取work 紀錄(主要是組成javascripts中的陣列回傳給)
	public String GetRFPworkbyUser(String user) throws JSONException,
			ParseException, UnsupportedEncodingException {
		String result = "[";
		// PreparedStatement stmt = null;
		Statement stmt = null;
		ResultSet rs = null;
		String SQL = "select t1.work_number_id,t2.is_question,t1.insert_date, t1.finish_date  ,t1.apply_person_id ,t3.employee_name,t1.correspond_person_id,t4.employee_name, t1.record_person_id, t5.employee_name,t1.work_number_od,t1.status,t6.work_number_description"
				+ " from (( (public.work_employee_mapping t1 left join work_number_profile t2  on t1.work_number_id= t2.work_number_id ) left join public.employee_work_profile t3"
				+ " ON t3.employee_id=t1.apply_person_id ) left join public.employee_work_profile t4 ON t4.employee_id=t1.correspond_person_id)"
				+ " left join public.employee_work_profile t5 ON t5.employee_id=t1.record_person_id"
				+" left join public.work_number_profile t6 ON t6.work_number_id=t1.work_number_id"
				+ " where ( t1.correspond_person_id='"
				+ user
				+"' or ( t1.apply_person_id='"+user+"' and ( status='E' or status='F')))"
				+ "  and t1.finish_date is null order by t1.work_number_id  ASC";

		try {
			stmt = connection.createStatement();
			rs = stmt.executeQuery(SQL);
			int index = 0;
			while (rs.next()) {
				String temp = "";

				for (int i = 1; i < 14; i++) {
					if (i == 1) {
						temp = "[" + "'" + rs.getString(i) + "'";
					} else if (i == 13) {
						temp = temp + ",'" + java.net.URLEncoder.encode(rs.getString(i), "UTF-8") + "'" + "]";
					} else {
						if (i == 6 || i == 8 || i == 10) {
						} else {
							if (i == 5 || i == 7 || i == 9) {
								temp = temp + "," + "'" + rs.getString(i) + "_"
										+ rs.getString(i + 1) + "'";
							} else {
								if (i == 3) {
									temp = temp
											+ ","
											+ "'"
											+ TimeTools
													.convertDateTOYYYYMMDD(rs
															.getString(i))
											+ "'";
								} else if (i == 2) {
									String isquestion = "需求單";
									if (rs.getBoolean(i)) {
										isquestion = "問題單";
									}
									temp = temp + "," + "'" + isquestion + "'";
								} else {
									temp = temp + "," + "'" + rs.getString(i)
											+ "'";
								}
							}
						}
					}
				}
				if (index != 0) {
					result = result + "," + temp;
				} else {
					result = result + temp;
				}
				index++;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {

				rs.close();
				stmt.close();
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result + "]";

	}

	// 動態從資料庫選取work 紀錄(主要是組成javascripts中的陣列回傳給)
	public String GetRFPworkbyWorkNumber(String number) throws JSONException,
			ParseException {
		String result = "[";
		Statement stmt = null;
		ResultSet rs = null;

		String SQL = "select t1.work_number_id,t2.is_question,t1.insert_date, t1.finish_date  ,t1.apply_person_id ,t3.employee_name,t1.correspond_person_id,t4.employee_name, t1.record_person_id, t5.employee_name,t1.work_number_od,t1.status,t1.before_od"
				+ " from (( (public.work_employee_mapping t1 left join work_number_profile t2  on t1.work_number_id= t2.work_number_id ) left join public.employee_work_profile t3"
				+ " ON t3.employee_id=t1.apply_person_id ) left join public.employee_work_profile t4 ON t4.employee_id=t1.correspond_person_id)"
				+ " left join public.employee_work_profile t5 ON t5.employee_id=t1.record_person_id"
				+ " where t1.work_number_id='"
				+ number
				+ "' order by t1.work_number_od desc";
		try {
			stmt = connection.createStatement();
			rs = stmt.executeQuery(SQL);
			int index = 0;
			while (rs.next()) {
				String temp = "";

				for (int i = 1; i < 14; i++) {
					if (i == 1) {
						temp = "[" + "'" + rs.getString(i) + "'";
					}else if(i==2){
						String isquestion = "需求單";
						if (rs.getBoolean(i)) {
							isquestion = "問題單";
						}
						temp = temp + "," + "'" + isquestion + "'";
					
					} else if (i == 13) {
						temp = temp + ",'" + rs.getString(i) + "'" + "]";
					} else {
						if (i == 6 || i == 8 || i == 10) {
						} else {
							if (i == 5 || i == 7 || i == 9) {
								temp = temp + "," + "'" + rs.getString(i) + "_"
										+ rs.getString(i + 1) + "'";
							} else {
								if (i == 3 || i == 4) {
									if (rs.getString(i) != null) {
										temp = temp
												+ ","
												+ "'"
												+ TimeTools
														.convertDateTOYYYYMMDD(rs
																.getString(i))
												+ "'";
									} else {
										temp = temp + "," + "'"
												+ rs.getString(i) + "'";
									}
								} else if (i == 2) {
									temp = temp + "," + "'" + rs.getBoolean(i)
											+ "'";
								} else {
									temp = temp + "," + "'" + rs.getString(i)
											+ "'";
								}
							}
						}
					}
				}
				if (index != 0) {
					result = result + "," + temp;
				} else {
					result = result + temp;
				}
				index++;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result + "]";
	}

	// 動態從資料庫選取work 紀錄(主要是組成javascripts中的陣列回傳給)
	public String GetRFPworkbyWorkNumberandStatus(String number, String status,int od)
			throws JSONException, ParseException, UnsupportedEncodingException {
		String result = "[";
		Statement stmt = null;
		ResultSet rs = null;

		String SQL = "select t1.work_number_id, t2.is_question,t1.insert_date, t1.finish_date  ,t1.apply_person_id ,t3.employee_name,t1.correspond_person_id,t4.employee_name, t1.record_person_id, t5.employee_name,t1.work_number_od,t1.status,t1.real_hours,t1.comment"
				+ " from (( (public.work_employee_mapping t1 left join work_number_profile t2  on t1.work_number_id= t2.work_number_id ) left join public.employee_work_profile t3"
				+ " ON t3.employee_id=t1.apply_person_id ) left join public.employee_work_profile t4 ON t4.employee_id=t1.correspond_person_id)"
				+ " left join public.employee_work_profile t5 ON t5.employee_id=t1.record_person_id"
				+ " where t1.work_number_id='"
				+ number
				+ "' and t1.status='"
				+ status 
				+ "' and t1.work_number_od="
				+od
				+ " order by t1.work_number_od desc";
		try {
			stmt = connection.createStatement();
			rs = stmt.executeQuery(SQL);
			int index = 0;
			while (rs.next()) {
				String temp = "";

				for (int i = 1; i < 15; i++) {
					if (i == 1) {
						temp = "[" + "'" + rs.getString(i) + "'";
					} else if (i == 14) {
						if(rs.getString(i)!=null){
							temp = temp + ",'"+ java.net.URLEncoder.encode(rs.getString(i), "UTF-8")+ "'" + "]";
						}else{
							temp = temp + ",'"+ rs.getString(i)+ "'" + "]";
						}
					} else {
						if (i == 6 || i == 8 || i == 10) {
						} else {
							if (i == 5 || i == 7 || i == 9) {
								temp = temp + "," + "'" + rs.getString(i) + "_"
										+ rs.getString(i + 1) + "'";
							} else {
								if (i == 3 || i == 4) {
									if (rs.getString(i) != null) {
										temp = temp
												+ ","
												+ "'"
												+ TimeTools
														.convertDateTOYYYYMMDD(rs
																.getString(i))
												+ "'";
									} else {
										temp = temp + "," + "'"
												+ rs.getString(i) + "'";
									}
								} else if (i == 2) {
//									 String isquestion="問題單";
//									 if(!rs.getBoolean(i)){
//									 isquestion="需求單";
//									 }
									temp = temp + "," + "'" + rs.getBoolean(i)
											+ "'";
								} else {
									temp = temp + "," + "'" + rs.getString(i)
											+ "'";
								}
							}
						}
					}
				}
				if (index != 0) {
					result = result + "," + temp;
				} else {
					result = result + temp;
				}
				index++;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result + "]";

	}

	// 動態從資料庫選取job log紀錄(主要是組成javascripts中的陣列回傳給)
	public String GetAllJOBLog() throws JSONException {

		String result = "[";

		Statement stmt = null;
		ResultSet rs = null;

		String SQL = "select * from public.execute_job_log";
		try {
			stmt = connection.createStatement();
			rs = stmt.executeQuery(SQL);
			int index = 0;
			while (rs.next()) {
				String temp = "";
				for (int i = 1; i < 8; i++) {
					if (i == 1) {
						temp = "[" + "'" + rs.getString(i) + "'";
					} else if (i == 7) {
						temp = temp + ",'" + rs.getString(i) + "'" + "]";
					} else {
						temp = temp + "," + "'" + rs.getString(i) + "'";
					}
					// templist.add(rs.getString(i));

				}
				// resultList.add(templist.);
				if (index != 0) {
					result = result + "," + temp;
				} else {
					result = result + temp;
				}
				index++;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {

				rs.close();
				stmt.close();
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result + "]";

	}

	// 動態從資料庫選取job log紀錄
	// job_id character varying NOT NULL, -- job名稱
	// job_begin_time timestamp without time zone NOT NULL, -- job啟動時間
	// pgm_id character varying NOT NULL, -- 程式名稱
	// pgm_begin_time timestamp without time zone NOT NULL, -- 程式啟動時間
	// pgm_end_time timestamp without time zone, -- 程式結束時間

	// status character varying(1), -- 目前狀態, 1=執行中, 0=已結束
	public String GetjobprocessLog(String jobName, String jobStartTime)
			throws JSONException {
		String result = "[";
		PreparedStatement stmt = null;
		ResultSet rs = null;

		String SQL = "select pgm_id,pgm_begin_time,pgm_end_time,status from public.execute_pgm_log where job_id=? and job_begin_time=? ";
		try {

			stmt = connection.prepareStatement(SQL);
			stmt.setString(1, jobName);
			stmt.setString(2, jobStartTime);
			rs = stmt.executeQuery();
			int index = 0;
			while (rs.next()) {
				String temp = "";
				for (int i = 1; i < 5; i++) {
					if (i == 1) {
						temp = "[" + "'" + rs.getString(i) + "'";
					} else if (i == 4) {
						temp = temp + ",'" + rs.getString(i) + "'" + "]";
					} else {
						temp = temp + "," + "'" + rs.getString(i) + "'";
					}
				}
				if (index != 0) {
					result = result + "," + temp;
				} else {
					result = result + temp;
				}
				index++;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				stmt.close();
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result + "]";

	}
}
