package kofman.twbbs.org.api;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import newmusiclif.com.properties.PropertiesCenter;
import newmusiclif.com.properties.PropertiesUtil;
import newmusiclife.com.dao.PropertiesDao;
import newmusiclife.com.dao.tokenDAO;
import newmusiclife.com.vo.tokenVO;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

import tw.com.tej.tools.RequestBODY;
import tw.com.tej.tools.httpTools;
import tw.com.tej.tools.toolsUtil;


@Path("/google")
public class GoogleCheck {


	private static String generateSessionId()
			throws UnsupportedEncodingException {
		String uid = new java.rmi.server.UID().toString(); // guaranteed unique
		return URLEncoder.encode(uid, "UTF-8"); // encode any special chars
	}
	
	static final String platform="google";
	
	@POST
	@Path("/register")
	@Consumes("application/json")
	//@Consumes("text/plain;charset=utf-8")
	@Produces(MediaType.TEXT_PLAIN)
	public String requestVersionAndTicket2(@Context HttpServletRequest request,
			@Context HttpServletResponse res,String params)
			{
		System.out.println("register POST is comming!!");
		HashMap<String, String> parameters;

		JSONObject respJSON = new JSONObject();
		
		
		JSONObject result=new JSONObject();
		try{
			JSONObject requestJSON = new JSONObject(params);
			String regId=requestJSON.getString("regId");
			String deviceName=requestJSON.getString("deviceName");
			String androidId=requestJSON.getString("androidId");
			
			String time=requestJSON.getString("time");
			String sign=requestJSON.getString("sign");
			
			String md5=toolsUtil.getInstance().md5(regId+deviceName+androidId+time);
			System.out.println("compare MD5:"+md5+" with sign:"+sign);
			if(!StringUtils.isEmpty(sign)&&!StringUtils.isEmpty(md5)&&md5.equals(sign)){
				//驗證成功
				tokenVO vo=new tokenVO();
				vo.setId(androidId);
				vo.setPlatform(platform);
				vo.setToken(regId);
				tokenDAO.getInstance().checkRecord(vo);
				result.put("status", "-100");
				result.put("msg", "Success");
			}else{
				result.put("status", "-101");
				result.put("msg", "Error");
			}
			System.out.println(requestJSON.get("regId"));
			result.put("regId", requestJSON.get("regId"));
			result.put("deviceName",requestJSON.get("deviceName"));
			result.put("androidId",requestJSON.get("androidId"));
		
		}catch(Exception e){
			System.out.println(e.toString());
			e.printStackTrace();
			try{
			result.put("status", "-999");
			result.put("msg", "JSONException Error");
			}catch(JSONException e1){

			}
		}
		System.out.println(result.toString());
		//getToken(request, oauth_token, oauth_verifier, oauth_token_secret_1)

		//result=getToken(request,oauth_token, oauth_verifier,(String)pro.get("oauth_token_secret"));
	
		return result.toString();
}
	
	
	@GET
	@Path("/registerTest")
	@Consumes("application/json")
	@Produces(MediaType.TEXT_PLAIN)
	public String requestVersionAndTicket3(@Context HttpServletRequest request,
			@Context HttpServletResponse res, InputStream requestBodyStream)
			throws JSONException, UnsupportedEncodingException, NamingException {
		System.out.println("register GET is comming!!");
		
//		String oauth_token=request.getParameter("oauth_token");
//		String oauth_verifier=request.getParameter("oauth_verifier");
		JSONObject result=new JSONObject();
	              
		System.out.println(result);
		//getToken(request, oauth_token, oauth_verifier, oauth_token_secret_1)

		//result=getToken(request,oauth_token, oauth_verifier,(String)pro.get("oauth_token_secret"));
	
		return result.toString();
}

	@POST
	@Path("/send")
	@Consumes("application/json")
	@Produces(MediaType.TEXT_PLAIN)
	public String sendToUser(@Context HttpServletRequest request,
			@Context HttpServletResponse res,String params)
			 {
		System.out.println("send User is comming!!");
		try{
		JSONObject jsonObject = new JSONObject(params);
		String userMessage=jsonObject.getString("userMessage");
		
		List<tokenVO> listVo=tokenDAO.getInstance().selectByPlatform(platform);
		System.out.println("Size is:"+listVo.size());
		if(listVo.size()>0){
			String google_api_key=PropertiesUtil.getInstance().getPropertiy("gcm_api_key");
//			System.out.println("gcm_api_key:"+google_api_key);
			Sender sender=new Sender(google_api_key);
			Message message=new Message.Builder()
			.collapseKey("GCM_Message")
			.timeToLive(30)
			.delayWhileIdle(true)
			.addData("message", userMessage)
			.build();
			 for (int i = 0; i < listVo.size(); i++) {
				 tokenVO vo= listVo.get(i);
			
			try {
			    // use this for multicast messages.  The second parameter
			    // of sender.send() will need to be an array of register ids.
			    Result result = sender.send(message,vo.getToken(), 1);
			    System.out.println("Send to "+vo.getId()+" result:"+result);
			     //result.getMessageId();
			    if (result!= null) {
			        String canonicalRegId = result.getCanonicalRegistrationId();
			        String msgId=result.getMessageId();
			        System.out.println("Broadcast success: canonicalRegId:" + canonicalRegId+" msgId:"+msgId);

			    } else {
			        String error = result.getErrorCodeName();
			        System.out.println("Broadcast failure: " + error);
			    }
			     
			} catch (Exception e) {
			    e.printStackTrace();
			}
		
			 }}
		}catch(Exception e){
			e.printStackTrace();
		}
		String result="";
		System.out.println("Send Done");
		return result.toString();
}
	
	
    public static String convertStreamToString(InputStream is) throws UnsupportedEncodingException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"utf-8"));
    StringBuilder sb = new StringBuilder();
    String line = null;
    try {
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
    } catch (IOException e) {
    } finally {
        try {
            is.close();
        } catch (IOException e) {
        }
    }
    return sb.toString();
}

	
}
