package kofman.twbbs.org.api;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Properties;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

import tw.com.tej.tools.RequestBODY;
import tw.com.tej.tools.httpTools;


@Path("/ios")
public class IosPushApi {


	String platfrom="ios";
	private static String generateSessionId()
			throws UnsupportedEncodingException {
		String uid = new java.rmi.server.UID().toString(); // guaranteed unique
		return URLEncoder.encode(uid, "UTF-8"); // encode any special chars
	}
	
	
	@POST
	@Path("/register")
	@Consumes("application/json")
	@Produces(MediaType.TEXT_PLAIN)
	public String requestVersionAndTicket2(@Context HttpServletRequest request,
			@Context HttpServletResponse res, InputStream requestBodyStream)
			throws JSONException, UnsupportedEncodingException, NamingException {
		System.out.println("register is comming!!");
//		String oauth_token=request.getParameter("oauth_token");
//		String oauth_verifier=request.getParameter("oauth_verifier");
		JSONObject result=new JSONObject();
	              
		System.out.println(result);
		//getToken(request, oauth_token, oauth_verifier, oauth_token_secret_1)

		//result=getToken(request,oauth_token, oauth_verifier,(String)pro.get("oauth_token_secret"));
	
		return result.toString();
}
	
	
	@GET
	@Path("/registerTest")
	@Consumes("application/json")
	@Produces(MediaType.TEXT_PLAIN)
	public String requestVersionAndTicket3(@Context HttpServletRequest request,
			@Context HttpServletResponse res, InputStream requestBodyStream)
			throws JSONException, UnsupportedEncodingException, NamingException {
		System.out.println("register is comming!!");
//		String oauth_token=request.getParameter("oauth_token");
//		String oauth_verifier=request.getParameter("oauth_verifier");
		JSONObject result=new JSONObject();
	              
		System.out.println(result);
		//getToken(request, oauth_token, oauth_verifier, oauth_token_secret_1)

		//result=getToken(request,oauth_token, oauth_verifier,(String)pro.get("oauth_token_secret"));
	
		return result.toString();
}

	@POST
	@Path("/send")
	@Consumes("application/json")
	@Produces(MediaType.TEXT_PLAIN)
	public String sendToUser(@Context HttpServletRequest request,
			@Context HttpServletResponse res, InputStream requestBodyStream)
			throws JSONException, UnsupportedEncodingException, NamingException {
		System.out.println("send User is comming!!");
		String google_api_key="AIzaSyBz1CCu-6KOVrCcSlLTZA2R9oZill8j_Cw";
		Sender sender=new Sender(google_api_key);
		Message message=new Message.Builder().build();
		Result sender_result=sender.send(arg0, arg1, arg2);
		String msg=sender_result.getMessageId();
		String error=sender_result.getErrorCodeName();
//		String oauth_token=request.getParameter("oauth_token");
//		String oauth_verifier=request.getParameter("oauth_verifier");
		//JSONObject result=new JSONObject();
	              
		//System.out.println(result);
		//getToken(request, oauth_token, oauth_verifier, oauth_token_secret_1)

		//result=getToken(request,oauth_token, oauth_verifier,(String)pro.get("oauth_token_secret"));
	
		return result.toString();
}
	
	
    public static String convertStreamToString(InputStream is) throws UnsupportedEncodingException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"utf-8"));
    StringBuilder sb = new StringBuilder();
    String line = null;
    try {
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
    } catch (IOException e) {
    } finally {
        try {
            is.close();
        } catch (IOException e) {
        }
    }
    return sb.toString();
}

	
}
