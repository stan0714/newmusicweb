package newmusiclife.com.dao;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

public abstract class BaseDAO
{
	//public static final String DATA_SOURCE_DB = "java:comp/env/jdbc/mygame";
	public static final String DATA_SOURCE_DB = "java:comp/env/musiclife";							
	private static Map<String, DataSource> dsMap = new HashMap<String, DataSource>();
	private transient NamedParameterJdbcTemplate jdbcTemplate = null;
	private static final Log LOG = LogFactory.getLog(BaseDAO.class);

	abstract protected String getDsName();

	private NamedParameterJdbcTemplate getNamedParameterJdbcTemplate()
	{
		if(this.jdbcTemplate == null)
		{
			this.jdbcTemplate = new NamedParameterJdbcTemplate(getDataSource());
		}

		return this.jdbcTemplate;
	}

	private JdbcOperations getJdbcOperations()
	{
		return getNamedParameterJdbcTemplate().getJdbcOperations();
	}
	
	private SimpleJdbcCall getProcedureJdbcCall(String procedureName)
	{
		return new SimpleJdbcCall(getDataSource()).withProcedureName(procedureName);
	}

	protected DataSource getDataSource()
	{

		synchronized (dsMap)
		{
			DataSource dataSource = (javax.sql.DataSource) dsMap.get(getDsName());

			if(dataSource == null)
			{
				try
				{
					final InitialContext initContext = new InitialContext();
					dataSource = (javax.sql.DataSource) initContext.lookup(getDsName());
					dsMap.put(getDsName(), dataSource);
				}catch (NamingException e)
				{
					LOG.error("Get DataSource Error!", e);
				}
			}

			return dataSource;
		}
	}

	protected int queryForInt(final String sql, final Object... params)
	{
		return getJdbcOperations().queryForInt(sql, params);
	}

	protected int queryForInt(final String sql, BeanPropertySqlParameterSource paramSource)
	{
		return getNamedParameterJdbcTemplate().queryForInt(sql, paramSource);
	}

	protected int queryForInt(final String sql, Map<String, ?> paramMap)
	{
		return getNamedParameterJdbcTemplate().queryForInt(sql, paramMap);
	}

	protected long queryForLong(final String sql, final Object... params)
	{
		return getJdbcOperations().queryForLong(sql, params);
	}

	protected long queryForLong(final String sql, BeanPropertySqlParameterSource paramSource)
	{
		return getNamedParameterJdbcTemplate().queryForLong(sql, paramSource);
	}

	protected long queryForLong(final String sql, Map<String, ?> paramMap)
	{
		return getNamedParameterJdbcTemplate().queryForLong(sql, paramMap);
	}

	protected String queryForString(final String sql, final Object... params)
	{
		return getJdbcOperations().queryForObject(sql, String.class, params);
	}

	protected String queryForString(final String sql, BeanPropertySqlParameterSource paramSource)
	{
		return getNamedParameterJdbcTemplate().queryForObject(sql, paramSource, String.class);
	}

	protected String queryForString(final String sql, Map<String, ?> paramMap)
	{
		return getNamedParameterJdbcTemplate().queryForObject(sql, paramMap, String.class);
	}

	protected <T> T queryForObject(final String sql, final RowMapper<T> mapper, final Object... params)
	{
		try
		{
			return getJdbcOperations().queryForObject(sql, mapper, params);
		}catch (EmptyResultDataAccessException ex)
		{
			return null;
		}
	}

	protected <T> T queryForObject(final String sql, final RowMapper<T> mapper,
			BeanPropertySqlParameterSource paramSource)
	{
		try
		{
			return getNamedParameterJdbcTemplate().queryForObject(sql, paramSource, mapper);
		}catch (EmptyResultDataAccessException ex)
		{
			return null;
		}
	}

	protected <T> T queryForObject(final String sql, final RowMapper<T> mapper, Map<String, ?> paramMap)
	{
		try
		{
			return getNamedParameterJdbcTemplate().queryForObject(sql, paramMap, mapper);
		}catch (EmptyResultDataAccessException ex)
		{
			return null;
		}
	}

	protected <T> List<T> query(final String sql, final RowMapper<T> mapper, final Object... params)
	{
		return getJdbcOperations().query(sql, mapper, params);
	}

	protected <T> List<T> query(final String sql, final RowMapper<T> mapper, BeanPropertySqlParameterSource paramSource)
	{
		return getNamedParameterJdbcTemplate().query(sql, paramSource, mapper);
	}

	protected <T> List<T> query(final String sql, final RowMapper<T> mapper, Map<String, ?> paramMap)
	{
		return getNamedParameterJdbcTemplate().query(sql, paramMap, mapper);
	}

	protected List<Map<String, Object>> queryForList(final String methodName, final String sql, final Object... params)
	{
		return getJdbcOperations().queryForList(sql, params);
	}

	protected List<Map<String, Object>> queryForList(final String sql, BeanPropertySqlParameterSource paramSource)
	{
		return getNamedParameterJdbcTemplate().queryForList(sql, paramSource);
	}

	protected List<Map<String, Object>> queryForList(final String sql, Map<String, ?> paramMap)
	{
		return getNamedParameterJdbcTemplate().queryForList(sql, paramMap);
	}

	protected List<?> queryForList(final String sql, Class<?> classObj, final Object... params)
	{
		return (classObj == null) ? getJdbcOperations().queryForList(sql, params) : getJdbcOperations().queryForList(
				sql, params, classObj);
	}

	protected <T> List<T> queryForList(final String sql, Class<T> classObj, BeanPropertySqlParameterSource paramSource)
	{
		return getNamedParameterJdbcTemplate().queryForList(sql, paramSource, classObj);
	}

	protected <T> List<T> queryForList(final String sql, Class<T> classObj, Map<String, ?> paramMap)
	{
		return getNamedParameterJdbcTemplate().queryForList(sql, paramMap, classObj);
	}

	protected int execute(final String sql, final Object... params)
	{
		return getJdbcOperations().update(sql, params);
	}

	protected int execute(final String sql, Map<String, ?> paramMap)
	{
		return getNamedParameterJdbcTemplate().update(sql, paramMap);
	}
	
	protected KeyHolder execute(final String sql, BeanPropertySqlParameterSource paramSource)
	{
		KeyHolder keyholder = new GeneratedKeyHolder();
		getNamedParameterJdbcTemplate().update(sql, paramSource, keyholder);
		return keyholder;
	}
	
	protected Map<String,Object> executeProcedure(String procedureName, Map<String,?> params)
	{
		return getProcedureJdbcCall(procedureName).execute(params);
	}
	
	protected Map<String,Object> executeProcedure(String procedureName, SqlParameterSource parameterSource)
	{
		return getProcedureJdbcCall(procedureName).execute(parameterSource);
	}

	protected String boolean2YN(final boolean value)
	{
		return value ? "Y" : "N";
	}

	protected String checkErrorMsg(final String name, final int len)
	{
		return new StringBuffer().append("Èï∑Â∫¶Ë∂ÖÈÅéÈôêÂà∂").append(name).append(" ").append(len).toString();

	}
}
