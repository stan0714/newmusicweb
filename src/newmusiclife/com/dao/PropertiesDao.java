package newmusiclife.com.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import newmusiclife.com.vo.PropertiesVO;

import org.springframework.jdbc.core.BeanPropertyRowMapper;

public class PropertiesDao extends BaseDAO{

	private static PropertiesDao dao = null;
	private  HashMap<String,String> map=new HashMap<String, String>();
	List<PropertiesVO> PropertiesList;
	public static PropertiesDao getInstance() {
		if (dao == null)
			dao = new PropertiesDao();
		return dao;
	}

	@Override
	protected String getDsName() {
		// TODO Auto-generated method stub
		return DATA_SOURCE_DB;
	}

	private static final String SQL_SELECT_ALL = "SELECT * "
			   + "FROM parameter ";

public List<PropertiesVO> selectAll() {
// 執行查詢.
return query(SQL_SELECT_ALL, new BeanPropertyRowMapper<PropertiesVO>(PropertiesVO.class));
}





	

}
