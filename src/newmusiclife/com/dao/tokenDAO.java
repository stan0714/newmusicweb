package newmusiclife.com.dao;

import java.nio.channels.SelectableChannel;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;

import newmusiclife.com.vo.tokenVO;

public class tokenDAO extends BaseDAO {

	private static tokenDAO dao = null;

	public static tokenDAO getInstance() {
		if (dao == null)
			dao = new tokenDAO();
		return dao;
	}

	@Override
	protected String getDsName() {
		// TODO Auto-generated method stub
		return DATA_SOURCE_DB;
	}
	
	private static final String SQL_HAS_KEY="SELECT count(1) FROM token WHERE id=? AND platform=?";
	
	public boolean contains(tokenVO vo){
		return queryForInt(SQL_HAS_KEY, vo.getId(),vo.getPlatform())>0;
//		String id,String platform,String  token
	};
	
	
	private static final String SQL_SELECT_BY_KEY="SELECT * FROM token WHERE id=? AND platform=?";
	
	public tokenVO selecByKey(String id,String platform){
		return queryForObject(SQL_SELECT_BY_KEY, new BeanPropertyRowMapper<tokenVO>(tokenVO.class), id,platform);
	};
	
	
	private static final String SQL_SELECT_ALL="Select * FROM token ";
	public List<tokenVO> selectAll(){
		
		return query(SQL_SELECT_ALL, new BeanPropertyRowMapper<tokenVO>(tokenVO.class));
		
	}
	
	public static final String SQL_UPDATE="UPDATE token set column WHERE id=:id AND platform=:platform";
	
	public void update(tokenVO vo){
		if(vo==null)
			throw new IllegalArgumentException("VO 為空值");
		ArrayList<String> cretria=new ArrayList<String>();
		if(!StringUtils.isEmpty(vo.getId()));
	}
	
	public static final String SQL_INSERT="insert into  token (id,platform,token) values (:id,:platform,:token)"
			;
	
	public void insert(tokenVO vo){
		if(vo==null)
			throw new IllegalArgumentException("VO 為空值");
		execute(SQL_INSERT,new BeanPropertySqlParameterSource(vo));
	}
	
	public static final String SQL_UPDATE_TOKEN="update  token set token=:token where id=:id and platform=:platform"
			;
	
	public void updateToken(tokenVO vo){
		if(vo==null)
			throw new IllegalArgumentException("VO 為空值");
		execute(SQL_UPDATE_TOKEN, new BeanPropertySqlParameterSource(vo));
	}
	
	
	public void checkRecord(tokenVO vo){
		if(vo==null)
			throw new IllegalArgumentException("VO 為空值");
		if (contains(vo)) {
			update(vo);
		}else{
			insert(vo);
		}
	}
	
	private static final String SQL_SELECT_BY_Platform="SELECT * FROM token WHERE  platform=?";
	
	public List<tokenVO> selectByPlatform(String platform){
		
		return query(SQL_SELECT_BY_Platform, new BeanPropertyRowMapper<tokenVO>(tokenVO.class),platform);
		
	}
	
	
	
	
	
	
}
