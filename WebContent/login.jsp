<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>RFP系統登錄頁面</title>
<meta http-equiv="Content-Type" content="text/html;  charset=UTF-8"/>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/login.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="js/login.js"></script>
</head>
<body>
<div class="centered">
<table cellpadding="2px" cellspacing="1px" bgcolor="#F4F5F7" width="400px" class="tableBorder" align="center" >
    <tr>
        <td colspan="2" bgcolor="#0066FF">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" class="label">&nbsp;</td>
    </tr>
   
    <tr>
        <td align="center" colspan="2">
            <span><h1>歡迎登入RFP系統</h1></span>
        </td>
    </tr>                  
    <tr>
        <td colspan="2" class="label">&nbsp;</td>
    </tr>
    <tr>
        <td class="label" align="right" width="40%"><p>帳號:</p></td>
        <td align="left" width="60%"><input type="text" name="username"  id="user" maxlength="20"/></td>
    </tr>
    <tr>
        <td class="label" align="right"><p>密碼:<p></td>
        <td align="left"><input type="password" name="password" id="passwd" maxlength="20" /></td>
    </tr>
    <tr>
        <td class="label" align="right">&nbsp;</td>
        <td align="left"><input type="button" value="登入" id="login" /></td>
    </tr>                  
    <tr>
        <td colspan="2" class="label">&nbsp;</td>
    </tr>                
</table>
</div>                                                                                     
</body>
</html>