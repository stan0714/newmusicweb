<!DOCTYPE html>

<head>
<meta http-equiv="Content-Type" content="text/html;  charset=utf8">
<link rel="stylesheet" href="css/superfish.css" media="screen">
<link rel="stylesheet" href="css/login.css">
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type='text/javascript' src='js/superfish.js'></script>
<script>
	// initialise plugins
	jQuery(function() {
		jQuery('#example').superfish({
			useClick : true
		});
	});
</script>
</head>

<body>

	<div class="centered">
	<table>
	<ul class="sf-menu" id="example">
		<li class="current"><a href="#">新增需求單</a>
			<ul>
				<li><a href="#">menu item</a></li>
				<li class="current"><a href="#">long menu item
						sets sub width</a>
					<ul>
						<li class="current"><a href="#">menu item</a></li>
						<li><a href="#">menu item</a></li>
						<li><a href="#">menu item</a></li>

					</ul></li>
				<li><a href="#">查詢需求單</a>
					<ul>
						<li><a href="#">menu item</a></li>
						<li><a href="#">menu item</a></li>

					</ul></li>
				<li><a href="#">menu item</a>
					<ul>
						<li><a href="#">menu item</a></li>
						<li><a href="#">menu item</a></li>

					</ul></li>
			</ul></li>
		<li><a href="#">menu item 2</a></li>
		<li><a href="#">menu item 3</a><li>
		<li><a href="#">menu item 4</a></li>
	</ul>
	</table>
	</div>
</body>
</html>