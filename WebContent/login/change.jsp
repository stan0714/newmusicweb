<%@ page language="java" contentType="text/html; charset=UTF8"
	pageEncoding="UTF8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF8">
<title>新增工作編號</title>
<link rel="stylesheet" href="../css/jquery-ui-1.10.1.custom.css" type="text/css" />
<link rel="stylesheet" href="../css/open.css" type="text/css" />
<!-- include js -->
<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.10.1.custom.js"></script>
<script type="text/javascript" src="../js/JsSimpleDateFormat.js"></script>
<script type="text/javascript" src="../js/jquery.validate.js"></script>
<script type="text/javascript" src="../js/jquery.secret.min.js"></script>
<script type="text/javascript" src="../js/changepage.js"></script>
</head>
<body>
	<div style="margin-top: 4px;">
	<span>RFP編號:</span>
	<span><input id="number" name="number" value="" type="text"  style="width: 150px; " disabled="disabled" /></span>
	</div>
	<div style="margin-top: 4px;">
	<span>OD:</span>
	<span><input id="od_number" name="od_number" value="" type="text"  style="width: 50px;" disabled="disabled" /></span>
	</div>
	<form id="changeForm" class="cmxform">
		<div style="margin-top: 4px;">
		<span>申請人:</span>
	 <input type="text" id="apply_name" name="apply_name" disabled="disabled" />
		</div>
		<div style="margin-top: 4px;">
		<span>接單人:</span>
	 <input type="text" id="correspond_name" name="correspond_name" disabled="disabled"/>
		</div>

	<div style="margin-top: 4px;">
		<span>記錄人:</span>
	 <input type="text" id="record_name" name="record_name" disabled="disabled" />
		</div>
		<div style="margin-top: 4px;">
	 <span>狀態:</span>
			<select id="status" class="status" name="status" style="width: 150px;">
		</select>
		</div>
	<div style="margin-top: 4px;display:none;" id="selectarea">
	<span>負責人員:</span>
		<select id="dep" class="dep" name="dep" style="width: 150px;">	
		</select>
		<select id="person" class="person" name="person" style="width: 150px;">
		</select>
	<span><input id="addnewrow" type="button" value="新增負責人"/></span>
	</div>
	<div style="margin-top: 4px;" id="time">
	 <span>結束日期:</span>
	 <input type="text" id="now_status" name="now_status" />
		</div>
		<!--  
		<div style="margin-top: 4px;">
		<span>實際結束時間:</span>
	 <input type="text" id="datepicker" name="datepicker" />
	</div>
	-->
		<div style="margin-top: 4px;">
	<span>工時:</span>
	<span><input id="hour" name="hour" type="text"  style="width: 30px;" maxLength="5" /></span>
	</div>
	<div style="margin-top: 4px;">
					<label style="vertical-align:top">備註:</label>
					<textarea id="content" name="content" rows="3" cols="50"></textarea>
	</div>
	    <input type="submit" value="送出" />
		<!--  <input class="submit" id="submit" type="submit" value="送出"/>-->
	</form>
</body>
</html>