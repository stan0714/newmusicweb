<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../css/open.css" type="text/css" />
<link rel="stylesheet" href="../css/jquery-ui-1.10.1.custom.css"
	type="text/css" />
<link rel="stylesheet" href="../css/ui.jqgrid.css" type="text/css" />
<link rel="stylesheet" href="../css/superfish.css" media="screen">
<link rel="stylesheet" href="../css/jquery.tooltip.css" type="text/css" />
<link rel="stylesheet" href="../css/login.css" type="text/css" />
<script type="text/javascript" src="../js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.10.1.custom.js"></script>
<script type='text/javascript' src='../js/superfish.js'></script>
<script type="text/javascript" src="../js/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="../js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="../js/JsSimpleDateFormat.js"></script>
<script type="text/javascript" src="../js/jquery.validate.js"></script>
<script type="text/javascript" src="../js/jquery.secret.min.js"></script>
<script type="text/javascript" src="../js/mainpage.js"></script>
<script type="text/javascript" src="../js/jquery.tooltip.js"></script>
<script type="text/javascript" src="../js/querypage.js"></script>



<title>RFP系統查詢頁面</title>
</head>
<body>

	<div>
		<jsp:include page="head.jsp"></jsp:include>
	</div>
	<div style="clear: both;"></div>
	<div class="centerdiv">
		<INPUT TYPE=CHECKBOX NAME="number" id="checked_number" checked>
		<span id="rfp_number">
		<span>需求單號:</span> <span><input id="rfp_number_hinet"
			name="rfp_number_hinet" type="text" value="" /></span>
		</span>
		<div>
			<INPUT TYPE=CHECKBOX NAME="time" id="checked_time"> <span>時間:</span>
			<span id="rfp_time" style="display:none">
			<span><input id="rfp_time_value" name="rfp_user" type="text"
				value="" /></span>
			</span>
		</div>
		<div>
			<INPUT TYPE=CHECKBOX NAME="status" id="checked_status"> <span>單號狀態:</span>
			<span id="rfp_status" style="display:none">
			
			<select  class="status" name="status"
				style="width: 150px;">
				<option value="true">完成</option>
				<option value="false">未完成</option>
			</select>
			</span>
		</div>
		<div>
			<INPUT TYPE=CHECKBOX NAME="person" id="checked_person"> <span>負責人:</span>
			<span id="rfp_person" style="display:none">
			 <select  class="dep" name="dep"
				style="width: 150px;">
			</select> 
			<select  class="person" name="person"
				style="width: auto;">
			</select>
			</span>
		</div>
		<input class="submit" id="query_submit" type="submit" value="送出" />
	</div>
	<div style="clear: both;"></div>
	<div id="grid_div" class="querycenterdiv" style="display: none">
		<table id="querygrid"></table>
	</div>

</body>
</html>