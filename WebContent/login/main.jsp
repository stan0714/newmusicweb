<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="../css/open.css" type="text/css" />
<link rel="stylesheet" href="../css/jquery-ui-1.10.1.custom.css" type="text/css" />
<link rel="stylesheet" href="../css/ui.jqgrid.css" type="text/css" />
<link rel="stylesheet" href="../css/superfish.css" media="screen">
<link rel="stylesheet" href="../css/jquery.tooltip.css" type="text/css" />
<link rel="stylesheet" href="../css/login.css" type="text/css" />
<script type="text/javascript" src="../js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.10.1.custom.js"></script>
<script type="text/javascript" src="../js/jquery.ui.datepicker-zh-TW.js"></script>
<script type='text/javascript' src='../js/superfish.js'></script>
<script type="text/javascript" src="../js/i18n/grid.locale-en.js"></script>
<script type="text/javascript" src="../js/jquery.jqGrid.min.js"></script>
<script type="text/javascript" src="../js/JsSimpleDateFormat.js"></script>
<script type="text/javascript" src="../js/jquery.validate.js"></script>
<script type="text/javascript" src="../js/jquery.secret.min.js"></script>
<script type="text/javascript" src="../js/jquery.tooltip.js"></script>
<script type="text/javascript" src="../js/mainpage.js"></script>

<title>RFP系統首頁</title>
</head>
<body>

	<div>
		<jsp:include page="head.jsp"></jsp:include>
	</div>
	<div style="clear: both;"></div>
	<div class="centerdiv">
		<table id="grid"></table>
	</div>
</body>
</html>