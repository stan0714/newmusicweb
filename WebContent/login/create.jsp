<%@ page language="java" contentType="text/html; charset=UTF8"
	pageEncoding="UTF8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF8">
<title>新增工作編號</title>
<link rel="stylesheet" href="../css/jquery-ui-1.10.1.custom.css" type="text/css" />
<link rel="stylesheet" href="../css/open.css" type="text/css" />
<!-- include js -->
<script type="text/javascript" src="../js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="../js/jquery-ui-1.10.1.custom.js"></script>
<script type="text/javascript" src="../js/JsSimpleDateFormat.js"></script>
<script type="text/javascript" src="../js/jquery.validate.js"></script>
<script type="text/javascript" src="../js/jquery.secret.min.js"></script>
<script type="text/javascript" src="../js/mainpage.js"></script>
</head>
<body>
	<p>
	<span>RFP編號:</span>
	<span><input id="number" name="number" value="RFP編號:" type="text"  style="width: 150px;" /></span>
	<span id="msg" style="display:none"></span>
	</p>
	<form id="commentForm" class="cmxform">
	<div style="margin-top: 4px;">
					<label style="vertical-align:top">RFP說明:</label>
					<textarea id="content" name="content" rows="3" cols="50"></textarea>
	</div>

	<div style="margin-top: 4px;">
	<div id="selectarea">
	<span>負責人員:</span>
		<select id="dep" class="dep" name="dep" style="width: 150px;">	
		</select>
		<select id="person" class="person" name="person" style="width: 150px;">
		</select>
	</div>
	<span><input id="addnewrow" type="button"/></span>
	</div>
		<div style="margin-top: 4px;">
		<span>預計結案時間:</span>
	 <input type="text" id="datepicker" name="datepicker" />
	</div>
		<div style="margin-top: 4px;">
	<span>預計工時:</span>
	<span><input id="hour" name="hour" type="text"  style="width: 30px;" maxLength="5" /></span>
	</div>
	    <input type="submit" value="送出" />
		<!--  <input class="submit" id="submit" type="submit" value="送出"/>-->
	</form>
</body>
</html>