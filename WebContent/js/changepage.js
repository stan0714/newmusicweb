$(document).ready(
		function() {
//			// 清空所有的欄位
//			$('input[type!="button"][type!="submit"], select, textarea')
//					.val('').blur();
			closeSelect();
			depChange();
			$("#addnewrow").click(function(){
				addNewInfoRow();
			});
			
			var record_name=$("#record_name").val();
			if(record_name!=""){
				$('#status').attr('disabled', true);
				$('#hour').attr('disabled', true);
				
				
			}else{
				$('#status').attr('disabled', false);
				$('#time').empty();
				$('#time').append(
				'<span>實際結束時間:</span>'
				 +'<input type="text" id="datepicker" name="datepicker" />'
				);
				showDatapick();
				status_selectevent(false,"A");
			}
//			$("#changeForm").validate({
//				rules : {
//					content : {
//						required : true
//					},
//					hour : {
//						required : true,
//						number : true
//					},
//					datepicker : {
//						required : true
//					}
//
//				},
//				messages : {
//					content : {
//						required : "必填欄位"
//					},
//					hour : {
//						required : "必填欄位",
//						number : "請填入數字"
//
//					},
//					datepicker : {
//						required : "必填欄位"
//					}
//
//				},
//				submitHandler : function(form) {
//					var gate = $.secret('out', 'number_gate');
//					if (!gate) {
//						showmesbox();
//					}
//				}
//			});
//			$("#number").blur(function() {
//				$('#msg').hide();
//				var rfp_number = $("#number").val();
//				if (rfp_number != "") {
//					$.secret('in', 'rfp_number', rfp_number);
//
//					check_rfp_number();
//					// }
//				} else {
//					$('#msg').html('<label class="input_error">必填欄位!</label>');
//					$.secret('in', 'number_gate', false);
//					$('#msg').show();
//				}
//			});

		});
//將選取的負責人員那行移除掉 榜在addNewInfoRow上
function removeSelectedRow(){
	$(".removethisrow").click(function(){
		$(this).parent().parent().remove();
	});
}



//新增負責人員
function addNewInfoRow(){
	var html = 	"<div name=newrow ><span>負責人員:</span>"
	+' <select class="dep" name="dep" style="width: 150px;"></select>'
	+' <select class="person" name="person" style="width: 150px;"></select>'
	+' <span><input class="removethisrow" type="button" value="移除此列"/></span></div>';
	$('#selectarea').append(html);
	var depart= $.secret('out', 'dep');
	var lastdep=$('.dep').last();
	$.each(depart, function(key, value) {
		lastdep.append(
				'<option value="' + key + '">' + value + '</option>');

	});
	var value = lastdep.val();
	var person=$('.person').last();
	var per = $.secret('out', 'person');
	var mail = $.secret('out', 'mail');
	var id = $.secret('out', 'id');
	var group1person = per[value];
	var group1mail = mail[value];
	var group1id = id[value];
	$.each(group1person, function(key2, value2) {
		person.append(
				'<option value="' + group1mail[key2] + '">'
						+ group1id[key2] + "_" + value2
						+ '</option>');
	});
	depChange();
	removeSelectedRow();

}

//確認新增問題(需求單號)dialog
function showmesbox() {
	var number = $("#number").val();
	var person = $('#person option:selected').text();
	var datepick = $("#datepicker").val();
	var html = '<div>單號:' + number + "</div>" + '<div>負責人:' + person + '</div>'
			+ '<div>預計完成日期:' + datepick + '</div>';

	$('<div></div>').appendTo('body').html(html).dialog({
		modal : true,
		title : '新增一筆工作單號',
		zIndex : 10000,
		autoOpen : true,
		width : 'auto',
		resizable : false,
		buttons : {
			Yes : function() {
				$(this).dialog("close");
			},
			No : function() {
				$(this).dialog("close");
			}
		},
		close : function(event, ui) {
			$(this).remove();
		}
	});
}
//將選取的rfp_number丟到後台
function check_rfp_number() {
	var number = $.secret('out', 'rfp_number');
	var requestJSON = {
		"rfp_number" : number,
	};
	if (number != "") {
		var Uri = "http://" + window.location.hostname
				+ ":8080/rfp/services/rfpnumber/check";
				$.ajax({
					type : "POST",
					url : Uri,
					data : JSON.stringify(requestJSON),
					contentType : "application/json",
					processData : false,
					async : false,
					dataType : "json",
					success : function(Jdata) {
						if (Jdata.status == 200) {
							$.secret('in', 'number_gate', Jdata.result);
							if (Jdata.result) {

								$('#msg')
										.html(
												'<label class="input_error">此單號已經存在!!</label>');

							} else {
								$('#msg')
										.html(
												'<label class="input_success">此單號可以用</label>');
							}
							$('#msg').show();
						}

					},
					complete : function() {

					},
					error : function() {

					}
				});
	}
};



//$(".dep").live("change" ,function(){
//	var file_path=$(this).val();
//	$(".fake_file_path").val(file_path);
//});

/*
 * 	B	IT設計中	
	C	IT程式中	
	D	IT測試中	
	E	產品再擬中	
	F	產品主測試中	
	G	產品主結案	結束狀態
	H	規格退回不做	結束狀態
	I	規格延期退回	結束狀態
*/
function status_selectevent(bol,sel) {
	var status=["J","A","B","C","D","E","F","G","H","I"];
	var name=["問題單已回覆","產品主交件","IT設計中","IT程式中","IT測試中","產品再擬中","產品主測試中","產品主結案","規格退回不做","規格延期退回"];
	var index=1;
	$("#status").empty();	
	$.each(status, function(key, value) {
		if(!bol){
			if(key>0){
				
			if(sel==value){
				index=key;
				$("#status").append(
						'<option value="' + key + 'SELECTED">' +value+"_"+ name[key] + '</option>');
			}else{
			$("#status").append(
					'<option value="' + key + '">' +value+"_"+ name[key] + '</option>');
			}
			}
		}else{
			if(key<2){
				if(sel==value){
					index=key;
					$("#status").append(
							'<option value="' + key + 'SELECTED">' +value+"_"+ name[key] + '</option>');
				}else{
				$("#status").append(
						'<option value="' + key + '">' + value+"_"+ name[key]+ '</option>');
				}
			}
		}
//    $("#status option[value='"+sel+"']").attr('selected', 'selected');
    });

	
	$(".status").on("change" ,function(){
				var number=this.value;
				if(bol){
					if(number==0){
						alert("END EVEVT");
					}
				}else{
					var check=(number==index);
					if(check){
						$("#selectarea").hide();
					}else{
						if(number>6){
							$("#selectarea").hide();
						}else{		
							$("#selectarea").show();
							depChange();
						}
					}
				}
			});
}


function depChange() {
	$(".dep").on("change" ,function(){
//	$('#dep').on(
//			'change',
//			function() {
//				$('#person').empty();
//				$(this).next("select").empty();
				var person=$(this).next("select");
				person.empty();
				var value = this.value;
				var per = $.secret('out', 'person');
				var mail = $.secret('out', 'mail');
				var id = $.secret('out', 'id');
				var group1person = per[value];
				var group1mail = mail[value];
				var group1id = id[value];
				$.each(group1person, function(key2, value2) {
					person.append(
							'<option value="' + group1mail[key2] + '">'
									+ group1id[key2] + "_" + value2
									+ '</option>');
				});
			});
}

function showDatapick() {
	$("#datepicker").datepicker({
		onSelect : function(date) {
		},
	});
}

function closeSelect() {
	var Uri = "http://" + window.location.hostname
			+ ":8080/rfp/services/person/get";
	$.ajax({
		type : "GET",
		url : Uri,
		contentType : "application/json",
		dataType : "json",
		success : function(Jdata) {
			var depart = Jdata.dep;
			$.secret('in', 'dep', depart);
			$.secret('in', 'person', Jdata.person);
			$.secret('in', 'mail', Jdata.mail);
			$.secret('in', 'id', Jdata.id);
			$.each(depart, function(key, value) {
				$('.dep').append(
						'<option value="' + key + '">' + value + '</option>');

			});
			var per = $.secret('out', 'person');
			var mail = $.secret('out', 'mail');
			var id = $.secret('out', 'id');
			var group1person = per[0];
			var group1mail = mail[0];
			var group1id = id[0];
			$.each(group1person, function(key2, value2) {
				$('#person').append(
						'<option value="' + group1mail[key2] + '">'
								+ group1id[key2] + "_" + value2 + '</option>');
			});

		},
		complete : function() {

		},
		error : function() {

		}
	});
};

