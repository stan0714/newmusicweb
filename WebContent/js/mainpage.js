//var data = [];

var packagename = "rfp";
var port = "8080";

$(document).ready(function() {
	if (typeof (JSON) == 'undefined') {
		$.getScript('../js/json2.js');
	}
	$('#example').superfish({
		useClick : true
	});
	
	

	
	var status = [ "J", "A", "B", "C", "D", "E", "F", "G", "H", "I" ];
	var name = [ "問題單已回覆", "交件", "IT設計", "IT程式", "IT測試", "規格再確認",
			"產品主測試中", "產品主結案", "規格退回不做", "規格延期退回" ];
	$.secret('in', 'status_index', status);
	$.secret('in', 'status_value', name);
	setTimeout('closeSelect()', 500);
	checkpath();
});

// 將選取的負責人員那行移除掉 榜在addNewInfoRow上
function removeSelectedRow() {
	$(".removethisrow").click(function() {
		$(this).parent().parent().remove();
	});
}

/*
 * 檢查path是否有main.jsp 如果有 則需要載入相關的evenet
 */

function checkpath() {
	var pathname = window.location.pathname;
	if (pathname.indexOf("login/main.jsp") > 0) {
		getunFinishWorkbyUserid();
		$("#grid").jqGrid("clearGridData", true).trigger("reloadGrid");
		$("#box").hide();
		getmask();
		setTimeout('creategrid()', 1000);
		$("#closebox").click(function() {
			$("#box").fadeOut();
		});
		unmask();
	}

}
/*
 * 新增負責人員 目前還缺 當載入時page時 需要把相對應事件綁上
 */
function addNewInfoRow() {
	var html = "<div name=newrow ><span>負責人員:</span>"
			+ ' <select class="dep" name="dep" style="width: 150px;"></select>'
			+ ' <select class="person" name="person" style="width: 150px;"></select>'
			+ ' <span><input class="removethisrow" type="button" value="移除此列"/></span></div>';
	$('#selectarea').append(html);
	var depart = $.secret('out', 'dep');
	var lastdep = $('.dep').last();
	$.each(depart, function(key, value) {
		lastdep.append('<option value="' + key + '">' + value + '</option>');

	});
	var value = lastdep.val();
	var person = $('.person').last();
	var per = $.secret('out', 'person');
	var mail = $.secret('out', 'mail');
	var id = $.secret('out', 'id');
	var group1person = per[value];
	var group1mail = mail[value];
	var group1id = id[value];
	$.each(group1person, function(key2, value2) {
		person.append('<option value="' + group1mail[key2] + '">'
				+ group1id[key2] + "_" + value2 + '</option>');
	});
	depChange();
	removeSelectedRow();
}


/*
 * 根據傳入的value值 決定前台要SHOW哪種相關的dailog
 */
function showmesbox(value) {
	GETDialog.dialog('close');
	var html = "";
	var number = $("#number").val();
	$.secret('in', 'rfp_number', number);
	var content = $("#content").val();
	var hour = $("#hour").val();
	var type = $("#type").val();
	$.secret('in', 'type', type);
	$.secret('in', 'hours', $("#hour").val());
	$.secret('in', 'content', content);
	if (value == "new") {
		var person = $('.person option:selected').text();
		var personmail = $('.person option:selected').val();

		$.secret('in', 'person', person);
		$.secret('in', 'personmail', personmail);
		$.secret('in', 'type', $("#type").val());
		var datepick = $.secret('out', 'datepicker');

		html = '<div>單號:' + number + "</div>" + '<div>負責人:' + person + '</div>'
				+ '<div>預計完成日期:' + datepick + '</div>';
		if (type == "false") {
			$.secret('in', 'title_type', '新增一筆需求單號');
		} else {
			$.secret('in', 'title_type', '新增一筆問題單號');
		}
	} else {
		html = '<div>單號:' + number + "</div>";
		$.secret('in', 'title_type', '確認狀態變更');
		var bol = $.secret('out', 'finish_gate');
		if (!bol) {
			var value_array = $.secret('out', 'detail_user_array');
			$.each(value_array, function(key, value) {
				html = html + '<div>負責人:' + value + "</div>";
			});
		}
		$.secret('in', 'hours', $("#od_number").val());
		var end_datapick = $("#end_datepicker").val();
		var start_datapick = $("#start_datepicker").val();
		$.secret('in', 'end_datepicker', end_datapick);
		$.secret('in', 'start_datepicker', start_datapick);
		var selectedName = $('#status').find(":selected").text();
		$.secret('in', 'new_status', selectedName);
		var ori_select = $.secret('out', 'ori_select');
		html = html + '<div>狀態變更:' + ori_select + '->' + selectedName
				+ '<div>工時:' + hour + '</div>' + '<div>實際開始日期:'
				+ start_datapick + '</div>' + '<div>實際完成日期:' + end_datapick
				+ '</div></div><div><textarea rows="3" cols="50" disabled>'
				+ content + '</textarea></div>';
	}
	$('<div></div>').appendTo('body').html(html).dialog({
		modal : true,
		title : $.secret('out', 'title_type'),
		zIndex : 10000,
		autoOpen : true,
		width : 'auto',
		resizable : false,
		buttons : {
			Yes : function() {
				if (value == "new") {
					addnewWorkNumbrID();
				} else {
					changeWorkNumbrID();
				}

				$(this).dialog("close");
			},
			No : function() {
				$(this).dialog("close");
			}
		},
		close : function(event, ui) {

			$("#createNew").empty();
			$(this).remove();

			if (value == "new"||value == "detail") {
				checkpath();
			}else{
				loadquerygrid();
			}
		}
	});
}

//query.jsp grid page所需要的式件

function loadquerygrid(){
	var number=$('#rfp_number_hinet').val();
	if(number!=""){
		getmask();
		$("#querygrid").jqGrid("clearGridData", true).trigger("reloadGrid");
		getunFinishWorkbyWorkNumber(number);
		$("#grid_div").show();
		setTimeout('querygrid()', 500);
		unmask();
	}else{
		alert("請輸入查詢單號");
	}
}

/*
 * 轉換Worknumber編號的function 如果是結束狀態 則需要將所有資訊一併更新
 * 
 */

function changeWorkNumbrID() {

	var number = $.secret('out', 'rfp_number');
	var bol = $.secret('out', 'finish_gate');
	var person = [];
	var mail = [];
	if (!bol) {
		person = $.secret('out', 'detail_user_array');
		mail = $.secret('out', 'detail_user_array_mail');
	}
	var startdatepick = $.secret('out', 'start_datepicker');
	var enddatepick = $.secret('out', 'end_datepicker');
	var odnumber = $.secret('out', 'od_number');
	var content = $.secret('out', 'content');
	var hours = $.secret('out', 'hours');
	var newstatus = $.secret('out', 'new_status');
	var requestJSON = {
		"rfp_number" : number,
		"person" : person,
		"mail" : mail,
		"startdate" : startdatepick,
		"enddate" : enddatepick,
		"content" : content,
		"odnumber" : odnumber,
		"hours" : hours,
		"newstatus" : newstatus
	};
	if (number != "") {
		var Uri = "http://" + window.location.hostname
				+ ":8080/rfp/services/rfpnumber/insertMuti";
		$.ajax({
			type : "POST",
			url : Uri,
			data : JSON.stringify(requestJSON),
			contentType : "application/json",
			processData : false,
			async : false,
			dataType : "json",
			success : function(Jdata) {
				if (Jdata.status == 200) {
					alert("新增成功");
					send_mail("change", requestJSON);
				}

			},
			complete : function() {

			},
			error : function() {

			}
		});
	}
}

/*
 *新增一筆Worknumber編號的function
 * 
 */


function addnewWorkNumbrID() {

	var number = $.secret('out', 'rfp_number');
	var person = $.secret('out', 'person');
	var mail = $.secret('out', 'personmail');
	var datepick = $.secret('out', 'datepicker');
	var content = $.secret('out', 'content');
	var type = $.secret('out', 'type');
	var hours = $.secret('out', 'hours');

	var requestJSON = {
		"rfp_number" : number,
		"person" : person,
		"mail" : mail,
		"date" : datepick,
		"content" : content,
		"type" : type,
		"hours" : hours,
		"newstatus" : "A"
	};

	if (number != "") {
		var Uri = "http://" + window.location.hostname
				+ ":8080/rfp/services/rfpnumber/insert";
		$.ajax({
			type : "POST",
			url : Uri,
			data : JSON.stringify(requestJSON),
			contentType : "application/json",
			processData : false,
			async : false,
			dataType : "json",
			success : function(Jdata) {
				if (Jdata.status == 200) {
					alert("新增成功");
					send_mail("new", requestJSON);
				}

			},
			complete : function() {

			},
			error : function() {

			}
		});
	}
}

/*
 * 在奘換或是新增啥後 通知相關連絡人
 * value:需要通知多人
 * requestJSON:mail中所需要的參數
 * 
 */

function send_mail(value, requestJSON) {
	var Uri = "";
	if (value == "new") {
		Uri = "http://" + window.location.hostname
				+ ":8080/rfp/services/mail/send";
	} else {
		Uri = "http://" + window.location.hostname
				+ ":8080/rfp/services/mail/sendMuti";
	}
	$.ajax({
		type : "POST",
		url : Uri,
		data : JSON.stringify(requestJSON),
		contentType : "application/json",
		processData : false,
		async : false,
		dataType : "json",
		success : function(Jdata) {
			if (Jdata.status == 200) {
				alert("mail發送成功");
			}else{
				alert("mail發送失敗");
			}

		},
		complete : function() {

		},
		error : function() {

		}
	});
}


/*
 * 檢查此rfp編號是否已經有備使用過
 */

function check_rfp_number() {
	var number = $.secret('out', 'rfp_number');
	var requestJSON = {
		"rfp_number" : number,
	};
	if (number != "") {
		var Uri = "http://" + window.location.hostname
				+ ":8080/rfp/services/rfpnumber/check";
		$
				.ajax({
					type : "POST",
					url : Uri,
					data : JSON.stringify(requestJSON),
					contentType : "application/json",
					processData : false,
					async : false,
					dataType : "json",
					success : function(Jdata) {
						if (Jdata.status == 200) {
							$.secret('in', 'number_gate', Jdata.result);
							if (Jdata.result) {

								$('#msg')
										.html(
												'<label class="input_error">此單號已經存在!!</label>');

							} else {
								$('#msg')
										.html(
												'<label class="input_success">此單號可以用</label>');
							}
							$('#msg').show();
						}

					},
					complete : function() {

					},
					error : function() {

					}
				});
	}
};

/*
 * dep selected變動時所引發的事件
 */

function depChange() {
	$('.dep').change(
			function() {
				var person = $(this).next("select");
				person.empty();
				var value = this.value;
				var per = $.secret('out', 'person');
				var mail = $.secret('out', 'mail');
				var id = $.secret('out', 'id');
				var group1person = per[value];
				var group1mail = mail[value];
				var group1id = id[value];
				$.each(group1person, function(key2, value2) {
					person.append('<option value="' + group1mail[key2] + '">'
							+ group1id[key2] + "_" + value2 + '</option>');
				});
			});
}

/*
 * 將datepicker秀出(最小日期為今天)
 * 
 */

function showDatapick() {
	var dateToday = new Date();
	$("#datepicker").datepicker({
		dateFormat : "yy-mm-dd",
		showOn : 'button',
		buttonText : '選擇日期',
		minDate : dateToday,
		onSelect : function(date) {
			$.secret('in', 'datepicker', date);
		},
	});
}




/*
 * 將start_datepicker與end_datepicker秀出(最大日期為今天)
 * 
 */

function showDatapickincludebefore() {
	var dateToday = new Date();
	$("#start_datepicker").datepicker({
		dateFormat : "yy-mm-dd",
		showOn : 'button',
		maxDate : dateToday,
		buttonText : '選擇日期',
		onSelect : function(date) {
			$.secret('in', 'start_datepicker', date);
			var startTime = date;
			var endTime = $.secret('out', 'end_datepicker');
			if (endTime != false) {
				if (startTime > endTime) {
					alert("請選擇正確開始日期");
				} else {
					$.secret('in', 'start_datepicker', date);
				}
			} else {
				$.secret('in', 'start_datepicker', date);
			}
		},
	});
	$("#end_datepicker").datepicker({
		dateFormat : "yy-mm-dd",
		showOn : 'button',
		maxDate : dateToday,
        changeYear : true,
        changeMonth : true,
		buttonText : '選擇日期',
		onSelect : function(date) {
			var endTime = date;
			var startTime = $.secret('out', 'start_datepicker');
			if (startTime != false) {
				if (startTime <= endTime) {
					$.secret('in', 'end_datepicker', date);
				} else {
					alert("請選擇正確結束日期");
				}
			} else {
				$.secret('in', 'end_datepicker', date);
			}
		},
	});

}

/*
 * 初始最初的dep and person兩個select
 * 
 */

function closeSelect() {
	var Uri = "http://" + window.location.hostname
			+ ":8080/rfp/services/person/get";
	$.ajax({
		type : "GET",
		url : Uri,
		contentType : "application/json",
		dataType : "json",
		success : function(Jdata) {
			var depart = Jdata.dep;
			$.secret('in', 'dep', depart);
			$.secret('in', 'person', Jdata.person);
			$.secret('in', 'mail', Jdata.mail);
			$.secret('in', 'id', Jdata.id);
			$.each(depart, function(key, value) {
				$('.dep').append(
						'<option value="' + key + '">' + value + '</option>');

			});
			var per = $.secret('out', 'person');
			var mail = $.secret('out', 'mail');
			var id = $.secret('out', 'id');
			var group1person = per[0];
			var group1mail = mail[0];
			var group1id = id[0];
			$.each(group1person, function(key2, value2) {
				$('.person').append(
						'<option value="' + group1mail[key2] + '">'
								+ group1id[key2] + "_" + value2 + '</option>');
			});

		},
		complete : function() {

		},
		error : function() {

		}
	});
};

/*
 * 菌傳入的參數不同 
 * value=new             新建RFP表格
 *       showrfpdetail   RFP單號詳情(修改期望時間)
 */

function showdialog(value) {
	$("#mask").show();
	$("#createNew").empty();
	GETDialog = $("#createNew");
	if (value == "new") {
		GETDialog.load('divcreate.jsp', createNewJOB).dialog({
			resizable : false,
			title : "新增需求(問題)單",
			modal : true,
			width : 'auto',
			close : function(event, ui) {
				$("#createNew").hide();
				$("#mask").hide();
			}
		});
		
	} else if(value == "showrfpdetail"){
		
		GETDialog.load('divrfpfixtime.jsp', fixhopetimeJOB).dialog({
			resizable : false,
			title : "修改預期時間",
			modal : true,
			width : 'auto',
			close : function(event, ui) {
				$("#createNew").hide();
				$("#mask").hide();
			}
		});
	
	} else {
		
		GETDialog.load('divchange.jsp', changeJOB).dialog({
			resizable : false,
			title : "詳細資訊",
			modal : true,
			width : 'auto',
			close : function(event, ui) {
				$("#createNew").hide();
				$("#mask").hide();
			}
		});

	}

}

function fixhopetimeJOB(){

	var detail = $.secret('out', 'numberdetail');
	$('#detail_number').val(detail[3]);
	$('#detail_date').html(detail[1]);
	$('#detail_content').html(detail[0]);
	showDatapick();
	if(detail[4]==null){
		$("#datepicker").datepicker('setDate', changeStringtoDate(detail[1]));
	}else{
		$("#datepicker").datepicker('setDate', changeStringtoDate(detail[4]));
	}
	
}
function changeStringtoDate(value){
	return new Date(Date.parse(value .replace(/-/g, "/")));
}



//將狀態變更事件綁上
function changeJOB() {
	var temp = $.secret('out', 'numberinfo');
	var info = temp.toString().split(',');
	fillupContext(info);
	closeSelect();
	depChange();
	$("div.item").tooltip();
	$('#changecorrespondname').click(function() {
		if($('#change_correspond_name').css('display') == 'none'){ 
			$('#change_correspond_name').show();
			$('#changecorrespondname').val("取消更換接單人");
			$('#status').attr('disabled', true);
		} else { 
			$('#change_correspond_name').hide();
			$('#changecorrespondname').val("更換接單人");
			$('#status').removeAttr('disabled');
		};
	});
	$('#fix_hope_time').click(function() {
		showdialog("showrfpdetail");
//		if($('#fix_div').css('display') == 'none'){ 
//			   $('#fix_div').show('slow'); 
//			} else { 
//			   $('#fix_div').hide('slow'); 
//			}
	});
	var record_name = $("#record_name").val();
	if (record_name != "") {
		$('#status').attr('disabled', true);
		$('#hour').attr('disabled', true);
	} else {
		$('#status').attr('disabled', false);
		$('#time').empty();
		$('#time')
				.append(
						'<div><span>實際開始時間:</span>'
								+ '<input type="text" id="start_datepicker" name="datepicker" disabled /><div>'
								+ '<div><span>實際結束時間:</span>'
								+ '<input type="text" id="end_datepicker" name="datepicker" disabled /></div>'

				);
		showDatapickincludebefore();

	}
	vaildForm("detail");
}

function fillupContext(info) {
	$('#number').val(info[0]);
	$('#od_number').val(info[7]);
	$.secret('in', 'od_number', info[7]);
	$('#apply_name').val(info[5]);
	$("#fixbuttondiv").hide();
	$('#correspond_name').val(info[4]);
	if (info[6] != "null_null") {
		$('#record_name').val(info[6]);
		$('#start_date').val(info[2]);
		$('#end_date').val(info[3]);
		$("#content").attr("disabled", true);
		$('#hour').val(info[9]);
		$("#content").val(decodeURIComponent(info[10].replace(/\+/g," ")));
		$("#submitbuttondiv").hide();
		$("#fixbuttondiv").show();
		$('#fix_hope_time').hide();
		fix_content();
	}
	status_selectevent(info[1], info[8]);
	$("#remark").html("目前狀態:"+$('#status :selected').text());
	showDatapickincludebefore();
	$("#addnewrow").click(function() {
		addNewInfoRow();
	});
	
	var detail = $.secret('out', 'numberdetail');
	$('#detail_number').html(info[0]);
	$('#detail_date').html(detail[1]);
	$('#detail_hour').html(detail[2]);
	$('#detail_content').html(detail[0]);
}

function fix_content(){
	$("#fix").click(function() {
		$("#content").removeAttr("disabled");
		$.secret('in', 'ori_context', $("#content").val());
		$("#fixbuttondiv").empty();
		$("#fixbuttondiv").append('<input id="sendfix" type="button" value="送出修改"/>');
		sendfix();
	});
}
function sendfix(){
	$("#sendfix").click(function() {
		var content=$("#content").val();
		var ori_content=$.secret('out', 'ori_context');
		if(content!=""&&content!=ori_content){
			var number=$("#number").val();
			var od=$("#od_number").val();
			updateComment(number,od,content);
		}else{
			alert("請檢查輸入內容");
		}
	});
}

/*
 * B IT設計中 C IT程式中 D IT測試中 E 產品再擬中 F 產品主測試中 G 產品主結案 結束狀態 H 規格退回不做 結束狀態 I 規格延期退回
 * 結束狀態
 */
function status_selectevent(bol, sel) {
	// 獎原本的se;
	$.secret('in', 'ori_select', sel);
	var status=$.secret('out', 'status_index');
	var name=$.secret('out', 'status_value');

	var index = 1;
	$("#status").empty();
	$.each(status, function(key, value) {
		if (bol != "false") {
			if (key < 2) {
				var check = (sel == value);
				if (check) {
					index = key;
					$("#status").append(
							'<option value="' + key + '" SELECTED>' + value
									+ "_" + name[key] + '</option>');
				} else {
					$("#status").append(
							'<option value="' + key + '">' + value + "_"
									+ name[key] + '</option>');
				}
			}
		} else {
			if (key > 0) {
				var check = (sel == value);
				if (check) {
					index = key;
					$("#status").append(
							'<option value="' + key + '" SELECTED>' + value
									+ "_" + name[key] + '</option>');
				} else {
					$("#status").append(
							'<option value="' + key + '">' + value + "_"
									+ name[key] + '</option>');
				}
			}

		}
	});

	$(".status").on("change", function() {
		var number = this.value;
		if (bol == "true") {
			if (number == 0) {
				$.secret('in', 'finish_gate', true);
			}
		} else {
			var check = (number == index);
			if (check) {
				$.secret('in', 'finish_gate', true);
				$("#selectarea").hide();
			} else {
				if (number > 6) {
					$.secret('in', 'finish_gate', true);
					$("#selectarea").hide();
				} else {
					$.secret('in', 'finish_gate', false);
					$("#selectarea").show();
					depChange();
				}
			}
		}
	});
}

function updateComment(number,od,content) {
	
	var requestJSON = {
			"rfp_number" : number,
			"od":od,
			"content":content
		};

	var Uri = "http://" + window.location.hostname
			+ ":8080/rfp/services/rfpnumber/updatecomment";
	$.ajax({
		type : "POST",
		url : Uri,
		contentType : "application/json",
		data : JSON.stringify(requestJSON),
		dataType : "json",
		success : function(Jdata) {
			data1 = true;
			if (Jdata.status == "200") {
				alert("修改成功");
				GETDialog.dialog('close');
			}
		},
		complete : function() {

		},
		error : function() {

		}
	});
	
	
}

/*
 * 丟Servelt到猴台去查詢使用者未完成的工作清單
 * 
 */

function getunFinishWorkbyUserid() {
	var Uri = "http://" + window.location.hostname
			+ ":8080/rfp/services/rfpnumber/queryunfinish";
	$.ajax({
		type : "GET",
		url : Uri,
		contentType : "application/json",
		dataType : "json",
		success : function(Jdata) {
			data1 = true;
			if (Jdata.status == "200") {
				$.secret('in', 'unfinshwork', eval(Jdata.array));
			}
		},
		complete : function() {

		},
		error : function() {

		}
	});
}




/*
 * 將Select出來的為完成的工作清單資料 丟在前台jqGrid的顯示
 * 
 */

function creategrid() {
	$("#grid")
			.jqGrid(
					{
						datatype : "local",
//						width : 900,
						height : 300,

						colNames : [ "需求單編號","需求單類型","OD" ,"開始時間", "結束時間","申請人", "接單人","記錄人","執行狀態","description" ],
						colModel : [
								{
									name : 'work_number_id',
									index : 'work_number_id',
									width : 150,
									cellattr: function (rowId, val, rawObject, cm, rdata) {
											
				                            return 'title="' + rawObject.description + '"';
				                        }
								},
								
								{
									name : 'work_number_type',
									index : 'work_number_type',
									width : 100,
								},
								{
									name : 'work_number_od',
									index : 'work_number_od',
									width : 30,
									sorttype : "int",
								},
								{
									name : 'Start_Time',
									index : 'Start_Time',
									width : 75,
									sorttype : function(cell) {
										var a = cell.split(':');
										return parseInt(a[2], 10) + parseInt(a[1], 10)
												* 60 + parseInt(a[0], 10) * 3600;
									}

								},
								{
									name : 'End_Time',
									index : 'End_Time',
									width : 75,
									sorttype : function(cell) {
										var a = cell.split(':');
										return parseInt(a[2], 10) + parseInt(a[1], 10)
												* 60 + parseInt(a[0], 10) * 3600;
									}
								},

								{

									name : 'applyperson',
									index : 'applyperson',
									width : 100,
								},

								{
									name : 'correspondperson',
									index : 'correspondperson',
									width : 100,
								},
								{
									name : 'recordperson',
									index : 'recordperson',
									width : 100,
								},
								{
									name : 'status',
									index : 'status',
									width : 75,
								
								},
								{
									name : 'description',
									index : 'description',
									hidden:true
								}
								],
								pager : '#ptoolbar',
								sortname : 'Start_Time',
								sortorder : "desc",
								caption : "未完成工作青單",

					});
	$("#toolbar").jqGrid('navGrid', '#ptoolbar', {
		del : false,
		add : false,
		edit : false,
		search : false
	});
	$("#toolbar").jqGrid('filterToolbar', {
		stringResult : true,
		searchOnEnter : false
	});

	
	
	var names = [ "work_number_id","work_number_type", "Start_Time", "End_Time", "applyperson",
	  			"correspondperson","recordperson","work_number_od","status" ,"description"];
	var mydata = [];

	var data = $.secret('out', 'unfinshwork');

	for ( var i = 0; i < data.length; i++) {
		mydata[i] = {};
		for ( var j = 0; j < data[i].length; j++) {
			if (data[i][j] == "null"||data[i][j] == "null_null") {
				mydata[i][names[j]] = " ";
			}else if(j==8){
				mydata[i][names[j]] = compareStatus(data[i][j]);
			}else if(j==data[i].length-1){
				//將後台的回傳值作urldecode
				mydata[i][names[j]] =  decodeURIComponent(data[i][j].replace(/\+/g," "));
			}else{
				mydata[i][names[j]] =  data[i][j];
			}
		}
	}

	for ( var i = 0; i <= mydata.length; i++) {
		$("#grid").jqGrid('addRowData', i + 1, mydata[i]);
	}
	/*
	 * $("#grid").jqGrid('setGridParam', {onSelectRow:
	 * function(rowid,iRow,iCol,e){alert('row clicked');}});
	 */
	$("#grid").jqGrid('setGridParam', {
		ondblClickRow : function(rowid, iRow, iCol, e) {

			var number = "";
			var status = "";
			var od="";
			$("#" + rowid).each(function(i) {
				$("td", this).each(function(j) {
					if (j == 0) {
						number = $(this).text();
					} else if (j == 8){
						status = $(this).text().split('_')[0];
					}else if(j==2){
						od= $(this).text();
					}

				});
			});
			
			getunFinishWorkbyWorkNumberandStatus(number, status,od);
		}
	});
}

/*
 * 將後台傳入的文字 作比較之後
 * 回傳詳細的資訊 EX: 傳入:A 回傳:A_產品主交件
 */


function compareStatus(nowstatus){
	var status=$.secret('out', 'status_index');
	var name=$.secret('out', 'status_value');

	for ( var j = 0; j < status.length; j++) {
		if(nowstatus==status[j]){
			return status[j]+"_"+name[j];
		}
	}
	
	
}


/*根據以下欄位 得到估作單號的相關資訊
 * number:工作單編號
 * status:狀態
 * od:工作單的OD流水號
 */

function getunFinishWorkbyWorkNumberandStatus(number, status,od) {
	var requestJSON = {
		"rfp_number" : number,
		"status" : status,
		"od":od
	};
	var Uri = "http://" + window.location.hostname
			+ ":8080/rfp/services/rfpnumber/querybyvalues";
	$.ajax({
		type : "POST",
		url : Uri,
		data : JSON.stringify(requestJSON),
		contentType : "application/json",
		dataType : "json",
		success : function(Jdata) {
			data1 = true;
			if (Jdata.status == "200") {
				$.secret('in', 'numberinfo', eval(Jdata.array));
				$.secret('in', 'numberdetail', eval(Jdata.detail));
				
				showdialog("detail");
			}
		},
		complete : function() {

		},
		error : function() {

		}
	});
}



/*
 * 初始一個RFP單號所需要的事件
*/

function createNewJOB() {
	//初始一個RFP單號要將原本的欄位清空
	$('input[type!="button"][type!="submit"], select, textarea').val('').blur();
	closeSelect();
	depChange();
	showDatapick();
	fillcreatehint();
	$("div.item").tooltip();
	vaildForm("new");
	$("#number").blur(function() {
		$('#msg').hide();
		var rfp_number = $("#number").val();
		if (rfp_number != "") {
			$.secret('in', 'rfp_number', rfp_number);
			check_rfp_number();
		} else {
			$('#msg').html('<label class="input_error">必填欄位!</label>');
			$.secret('in', 'number_gate', false);
			$('#msg').show();
		}
	});
}

function fillcreatehint(){
		var Uri = "http://" + window.location.hostname
		+ ":8080/rfp/services/rfpnumber/getcreatenumberhint";
	$.ajax({
	type : "GET",
	url : Uri,
	contentType : "application/json",
	processData : false,
	async : false,
	dataType : "json",
	success : function(Jdata) {
		if (Jdata.status == 200) {
			$('#detail').html(Jdata.hint);
		}
	
		},
	});

}
/*
 * 根據傳入的value 來判定要vaildForm是哪種類型
 * new:新創的RFPIDfrom
 * other:change form
 */

function vaildForm(value) {
	if (value == "new") {
		$("#commentForm").validate({
			rules : {
				content : {
					required : true
				},
				hour : {
					required : true,
					number : true
				},
				datepicker : {
					required : true
				}

			},
			messages : {
				content : {
					required : "必填欄位"
				},
				hour : {
					required : "必填欄位",
					number : "請填入數字"

				},
				datepicker : {
					required : "必填欄位"
				}

			},
			submitHandler : function(form) {
				GETDialog.dialog('close');
				var gate = $.secret('out', 'number_gate');
				var number = $("#number").val();

				if (number != "" && !gate) {
					var date = $.secret('out', 'datepicker');

					if (date != false) {
						showmesbox("new");
					} else {
						alert("請選擇日期");
					}
				}
			}
		});
	} else {
		$("#changeForm").validate({
			rules : {
				content : {
					required : true
				},
				hour : {
					required : true,
					number : true
				},

			},
			messages : {
				content : {
					required : "必填欄位"
				},
				hour : {
					required : "必填欄位",
					number : "請填入數字"

				},

			},
			submitHandler : function() {
				var selectedName = $('#status').find(":selected").text();
				var ori_select = $.secret('out', 'ori_select');
				var startTime = $.secret('out', 'start_datepicker');
				var endTime = $.secret('out', 'end_datepicker');

				if (selectedName.indexOf(ori_select) == -1) {
					var check=startTime<=endTime;
					if(check){
						getPersonInfo();
						showmesbox("detail");
					}else{
						alert("請檢查開始與結束時間!");
					}
				} else {
					alert("請變更狀態!!");
				}
			}
		});
	}
}

/*
 * 得到人員的相關資訊
 */

function getPersonInfo() {
	var values = [];
	var mails = [];
	$(".person").each(function() {
		var temp = $("option:selected", this).text();
		var mail = $("option:selected", this).val();
		var found = $.inArray(temp, values) > -1;
		if (!found) {
			values.push(temp);
			mails.push(mail);
		}

	});
	$.secret('in', 'detail_user_array', values);
	$.secret('in', 'detail_user_array_mail', mails);
}

//秀出遮罩與loading圖案
function getmask() {
	$("#overlay").show();
	$("#mask").fadeIn();
}
//隱藏遮罩與loading圖案
function unmask() {
	$("#overlay").hide();
	$("#mask").fadeOut();
}
