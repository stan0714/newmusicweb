$(document).ready(function() {

	if (typeof (JSON) == 'undefined') {
		$.getScript('js/json2.js');
	}


	checkbrowser();
	$("#passwd").keyup(function(event){
	    if(event.keyCode == 13){
	    	check();
	    }
	});
	$("#login").click(function() {
		check();
	});

});

function checkbrowser(){
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf ( "MSIE " );
    if(msie>-1){
  	  alert("Please change your browser to firefox or Chrome!");
    }
}

function check() {
	var username = $("#user").val();
	var passwd = $("#passwd").val();
	var requestJSON = {
		"user" : username,
		"passwd" : passwd
	};
	if (username != "" && passwd != "") {
		var Uri = "http://" + window.location.hostname
				+ ":8080/rfp/services/login/check";
		$.ajax({
			type : "POST",
			url : Uri,
			data : JSON.stringify(requestJSON),
			contentType : "application/json",
			processData : false,
			async : false,
			dataType : "json",
			success : function(Jdata) {
				if (Jdata.status == 200) {
					var url = 'login/main.jsp';
					window.location.href = url;
				} else {
					alert("查無相關登入資訊!!");
				}

			},
			complete : function() {

			},
			error : function() {

			}
		});
	} else {
		alert("帳號跟密碼不可以為空!");
	}
};

