//var data = [];

var packagename="rfp";
var port="8080";
var availableTags=[];
$(document).ready(function() {
	//初始下拉式選單
	$('#example').superfish({
		useClick : true
	});
	getmask();
	getrfpnumberList();
	
	setTimeout('autocom()', 500);
	
	$("#rfp_number_hinet").keyup(function(event){
	    if(event.keyCode == 13){
			loadquerygrid();
	    }
	});
	//綁定checkbox事件
	$( "input[type=checkbox]" ).on( "click", countChecked );
//	setTimeout('closeSelect()', 500);
	depChange();
	unmask();
	//當submit button click觸發的事件
	$("#query_submit").click(function() {
		$("input[type=checkbox]").each(function() {
			var name=$(this).attr("name");
			var checked=$(this).is(':checked');
//			alert(name+" "+checked);
		});
		loadquerygrid();
	});

});



function countChecked(){
	var name=$(this).attr("name");
	var checked=$(this).is(':checked');
	if(checked){
		openSelected(name);
	}else{
		closeSelected(name);
	}
}
//將頁面上的span id =rfp_name的關閉
function closeSelected(name){
	var id=$("#rfp_"+name) ;
	id.hide();
} 

//將頁面上的span id =rfp_name的開啟
function openSelected(name){
	var id=$("#rfp_"+name) ;
	id.show();
} 


function autocom(){
	 $( "#rfp_number_hinet" ).autocomplete({
		 source: availableTags,
			minChars: 0,
			max: 5,
			autoFill: true,
			mustMatch: true,
			matchContains: true,
			scrollHeight: 220,
		 });

}

//將DB中所有任務取出
function getrfpnumberList() {
		
	var Uri = "http://" + window.location.hostname
			+ ":8080/"+packagename+"/services/rfpnumber/autocom";
	$.ajax({
		type : "GET",
		url : Uri,
		contentType : "application/json",
		dataType : "json",

		success : function(Jdata) {
			if (Jdata.status == "200") {
				var name = Jdata.array;
				var commad=new Array();
				
				jQuery.each(name, function(i, v) {
					commad[i]=v;
				});
				availableTags=commad;
			}else if(Jdata.status == "404"){
				
			}
		},
		complete : function() {

		},
		error : function() {

		}
	});
}









function hidemask() {
	$("#mask").fadeOut(3000);
}



function getunFinishWorkbyWorkNumber(number) {
	// 將DB中所有job log取出
	var requestJSON = {
		"rfp_number" : number
	};
	var Uri = "http://" + window.location.hostname
			+ ":8080/rfp/services/rfpnumber/querybynumber";
	$.ajax({
		type : "POST",
		url : Uri,
		data : JSON.stringify(requestJSON),
		contentType : "application/json",
		dataType : "json",
		success : function(Jdata) {
			data1 = true;
			if (Jdata.status == "200") {
				$.secret('in', 'numberinfo', eval(Jdata.array));
				$.secret('in', 'numberdetail', Jdata.detail);
			}
		},
		complete : function() {

		},
		error : function() {

		}
	});
}

function querygrid() {

	$("#querygrid").jqGrid(
			{
				datatype : "local",
//				width : 900,
				height : 300,

				colNames : [ "需求單編號","需求單類型","OD" ,"開始時間", "結束時間","申請人", "接單人","記錄人", "執行狀態","父OD" ],
				colModel : [
						{
							name : 'work_number_id',
							index : 'work_number_id',
							width : 150,
						},
						
						{
							name : 'work_number_type',
							index : 'work_number_type',
							width : 100,
						},
						{
							name : 'work_number_od',
							index : 'work_number_od',
							width : 30,
							sorttype : "int",
						},
						{
							name : 'Start_Time',
							index : 'Start_Time',
							width : 75,
							sorttype : function(cell) {
								var a = cell.split(':');
								return parseInt(a[2], 10) + parseInt(a[1], 10)
										* 60 + parseInt(a[0], 10) * 3600;
							}

						},
						{
							name : 'End_Time',
							index : 'End_Time',
							width : 75,
							sorttype : function(cell) {
								var a = cell.split(':');
								return parseInt(a[2], 10) + parseInt(a[1], 10)
										* 60 + parseInt(a[0], 10) * 3600;
							}
						},

						{

							name : 'applyperson',
							index : 'applyperson',
							width : 100,
						},

						{
							name : 'correspondperson',
							index : 'correspondperson',
							width : 100,
						},
						{
							name : 'recordperson',
							index : 'recordperson',
							width : 100,
						},
						{
							name : 'status',
							index : 'status',
							width : 75,
						} ,
						{
							name : 'before_od',
							index : 'before_od',
							width : 75,
						} ],
				pager : '#ptoolbar',
				sortname : 'Start_Time',
				sortorder : "desc",
				caption : "查詢單號結果",
			});
	$("#toolbar").jqGrid('navGrid', '#ptoolbar', {
		del : false,
		add : false,
		edit : false,
		search : false
	});
	$("#toolbar").jqGrid('filterToolbar', {
		stringResult : true,
		searchOnEnter : false
	});

	var names = [ "work_number_id","work_number_type", "Start_Time", "End_Time", "applyperson",
			"correspondperson","recordperson","work_number_od","status","before_od" ];
	var mydata = [];
	
	var data = $.secret('out', 'numberinfo');

	for ( var i = 0; i < data.length; i++) {
		mydata[i] = {};
		for ( var j = 0; j < data[i].length; j++) {
			if (data[i][j] == "null"||data[i][j] == "null_null") {
				mydata[i][names[j]] = "";		
			}else if(j==8){
				mydata[i][names[j]] = compareStatus(data[i][j]);
			}else{
				mydata[i][names[j]] = data[i][j];
			}
		}
	}

	for ( var i = 0; i <= mydata.length; i++) {
		$("#querygrid").jqGrid('addRowData', i + 1, mydata[i]);
	}
	/*
	 * $("#grid").jqGrid('setGridParam', {onSelectRow:
	 * function(rowid,iRow,iCol,e){alert('row clicked');}});
	 */
	$("#querygrid").jqGrid('setGridParam', {
		ondblClickRow : function(rowid, iRow, iCol, e) {
			var number = "";
			var status="";
			var od="";
			$("#" + rowid).each(function(i) {
				$("td", this).each(function(j) {
					if (j == 0) {
						number = $(this).text();
					}else if (j == 8){
						status = $(this).text().split('_')[0];
					}else if(j==2){
						od=$(this).text();
					}
				});
			});
			getunFinishWorkbyWorkNumberandStatus(number,status,od);
		}
	});
}



function hideMaskandBox() {
	$("#box").fadeOut();
}
function getmask() {
	$("#overlay").show();
	$("#mask").fadeIn();
}
function unmask() {
	$("#overlay").hide();
	$("#mask").fadeOut();
}
